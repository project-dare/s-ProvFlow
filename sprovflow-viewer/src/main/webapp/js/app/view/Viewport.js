/**
 * The main application viewport, which displays the whole application
 * @extends Ext.Viewport
 */
deleteWorkflowURL = "portalurl/"
var networks = [{
        "abbr": "G",
        "name": "GEOSCOPE"
    }, {
        "abbr": "AU",
        "name": "Geoscience Australia"
    }, {
        "abbr": "AZ",
        "name": "ANZA Regional Network"
    }, {
        "abbr": "BK",
        "name": "Berkeley Digital Seismograph Network"
    }

];

var userState = {
    state: {
        currentUser: '',
        originalUser: '',
        isAdmin: false,
    },

    setState: function (newState) {
        Object.assign(this.state, newState)
    },

    setRoles: function () {
        this.setState({
            isAdmin: hasAdminRole()
        });
    },

    setCurrentUser: function (user) {
        this.setState({
            currentUser: user
        });
    },

    setOriginalUser: function (user) {
        if (keycloak && keycloak.tokenParsed) {
            this.setState({
                originalUser: parseKeycloakID()
            });
        } else {
            this.setState({
                originalUser: user
            });
        }
    },

    getCurrentUser: function () {
        return this.state.currentUser;
    },
    getOriginalUser: function () {
        return this.state.originalUser;
    },
    isAdmin: function () {
        return this.state.isAdmin;
    }
}

var initState = function (userId) {
    userState.setOriginalUser(userId);
    userState.setCurrentUser(userId);
    loadStores()
}

function parseKeycloakID() {
    if (keycloak && keycloak.tokenParsed) {
        var issuer = keycloak.tokenParsed || keycloak.refreshTokenParsed
        return `${keycloak.subject}@${issuer.iss}`
    }
    return
}

function parseKeycloakName() {
    if (keycloak && keycloak.tokenParsed) {
        var issuer = keycloak.tokenParsed || keycloak.refreshTokenParsed
        return `${issuer.preferred_username}`
    }
    return
}

function hasAdminRole() {
    // Passed in by docker:
    nonauth_admin = ADMIN_NAME
    keycloak_admin = KEYCLOAK_ADMIN_ROLE_NAME
    user = userState.getCurrentUser();

    return user && user === nonauth_admin ?
        true :
        (keycloak && keycloak.realmAccess) ?
        keycloak.realmAccess.roles.indexOf(keycloak_admin) > -1:
        false
}

function loadStores() {
    user = userState.getCurrentUser();
    workflowStore.getProxy().api.read = PROV_SERVICE_BASEURL + 'workflowexecutions?usernames=' + user;
    workflowStore.load();

    termStore.getProxy().api.read = PROV_SERVICE_BASEURL + '/terms?usernames=' + user + '&aggregationLevel=username',
    termStore.load();
}

function clearStores() {
    // Clear Runtime instance monitor:
    activityStore.data.clear();
    setRuntimeMonitorTitle()

    // Clear Data Products view
    artifactStore.removeAll();
    
    // Clear Graph viewer
    var context = document.getElementById("viewportprov").getContext("2d")
    context.clearRect(0, 0, 99999, 99999)
}


Ext.onReady(function () {
    if (keycloak && keycloak.token) {
        Ext.Ajax.setDefaultHeaders({
            "Authorization": 'Bearer ' + keycloak.token,
        })
        var userId = parseKeycloakID();
        userState.setRoles();
        initState(userId);
    }
    if (!keycloak) {
        signInModal.show()
    }
})

Ext.define('CF.view.button', {
    extend: 'Ext.button.Button',
    alias: 'widget.toolbarButton',
});

var renderImpersonateButton = function () {
    return userState.isAdmin() ?
        {
            xtype: 'toolbarButton',
            text: 'Impersonate',
            handler: function () {
                impersonateModal.show()
            }
        } :
        null;
}

var renderResetButton = function () {
    return userState.isAdmin() ?
        {
            xtype: 'toolbarButton',
            text: 'Reset',
            handler: function () {
                userState.setCurrentUser(userState.getOriginalUser())
                var username = parseKeycloakName() || userState.getOriginalUser()
                Ext.getCmp("Username").setText('User: ' + username);
                clearStores();
                loadStores();
            }
        } :
        null;
}


Ext.define('CF.view.Viewport', {
    extend: 'Ext.Viewport',
    layout: 'fit',

    requires: [
        'Ext.layout.container.Border',
        'Ext.resizer.Splitter',
        'CF.view.ResultsPane'
    ],

    initComponent: function () {
        var me = this;
        var LogoutButton;

        if (keycloak && keycloak.token) {
            LogoutButton = {
                xtype: 'toolbarButton',
                text: 'Log out',
                handler: function () {
                    keycloak.logout()
                }
            }
        }

        Ext.apply(me, {
            id: 'viewport',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                border: 'false',
                layout: 'border',
                defaults: {
                    split: true
                },
                items: [{
                    xtype: 'panel', // Earthquake & Station & Common
                    region: 'center',
                    border: false,
                    split: true,
                    autoScroll: true,
                    layout: {
                        type: 'border',
                        padding: 5
                    },
                    defaults: {
                        split: true
                    },
                    dockedItems: [{
                        xtype: 'toolbar',
                        id: 'viewerToolbar',
                        dock: 'top',
                        height: 30,
                        items: [{
                                xtype: 'tbtext',
                                text: `User: ${parseKeycloakName() || userState.getCurrentUser()}`,
                                id: 'Username',
                                style: 'font-weight: bold; color: #04408c',
                                centered: true,
                            },
                            '->', // Right align button
                            renderImpersonateButton(),
                            renderResetButton(),
                            LogoutButton
                        ]
                    }, ],
                    items: [
                        Ext.create('CF.view.ActivityMonitor'),
                        {
                            region: 'center',
                            layout: 'border',
                            border: false,
                            autoScroll: true,
                            items: [
                                Ext.create('CF.view.provenanceGraphsViewer'), //issue here
                                Ext.create('CF.view.ArtifactView')
                            ]
                        }
                    ]
                }]
            }]
        });
        me.callParent(arguments);
    }
});


selectedFile = "";

function fileSelection(filetype) {
    var user = getCurrentUser();

    Ext.Ajax.request({
        url: getListURL,
        params: {
            userSN: user,
            filetype: filetype
        },
        success: function (response) {
            showPopup(response.responseText, filetype);
        }
    });
}

function showPopup(htmlFileList, filetype) {
    var filelist = Ext.widget('panel', {
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        border: false,
        bodyPadding: 10,
        html: htmlFileList,
        buttons: [{
            text: 'Cancel',
            handler: function () {
                selectedFile = "";
                this.up('window').hide();
            }
        }, {
            text: 'Select',
            handler: function () {
                parseSelectedFile(this.up('window'), filetype);
            }
        }]
    });

    win = Ext.widget('window', {
        title: 'Select existing file',
        closeAction: 'hide',
        width: 300,
        height: 350,
        layout: 'fit',
        resizable: true,
        modal: true,
        items: filelist
    });
    win.show();
}

function parseSelectedFile(win, filetype) {
    if (selectedFile === "") {
        Ext.Msg.alert("Alert!", "Please, select a file by clicking on it");
    } else {
        if (filetype === EVENT_TYPE) getEvents(ctrl, selectedFile, new QuakeMLXMLFormat());
        if (filetype === STXML_TYPE) getStations(ctrl, selectedFile, new StationXMLFormat());
        if (filetype === STPOINTS_TYPE) getStations(ctrl, selectedFile, new PointsListFormat());
        selectedFile = "";
        win.hide();
    }
}

function selectFile(e) {
    $("li").css('background-color', '');
    selectedFile = e.getAttribute('filePath');
    $(e).css('background-color', '#CED9E7');
}

var signInHandler = function () {
    username = Ext.getCmp('signInModal').getValue();
    initState(username);
    userState.setRoles();
    Ext.getCmp("Username").setText('User: ' + userState.getCurrentUser());
    Ext.getCmp("viewerToolbar").add(renderImpersonateButton())
    Ext.getCmp("viewerToolbar").add(renderResetButton())
    Ext.getCmp('signInModalComponent').close();
}

var signInModal = Ext.create("Ext.Window", {
    title: 'User',
    id: 'signInModalComponent',
    width: 350,
    height: 130,
    modal: true,
    closable: false,
    items: [{
        xtype: 'textfield',
        fieldLabel: 'Username: ',
        labelAlign: 'right',
        name: 'username',
        anchor: '80%',
        id: 'signInModal',
        allowBlank: false,
        margin: '10 10 10 0',
        listeners: {
            specialkey: function (f, e) {
                if (e.getKey() == e.ENTER) {
                    signInHandler();
                }
            }
        }
    }],
    buttons: [{
        text: 'Ok',
        handler: function () {
            signInHandler();
        }
    }]
})

var impersonateHandler = function () {
    userId = Ext.getCmp('impersonateModal').getValue();
    userState.setCurrentUser(userId);
    Ext.getCmp("Username").setText('User: ' + userState.getCurrentUser());
    clearStores();
    loadStores();
    Ext.getCmp('impersonateModalComponent').hide();
}

var impersonateModal = Ext.create("Ext.Window", {
    title: 'Impersonate user',
    id: 'impersonateModalComponent',
    width: 350,
    height: 130,
    modal: true,
    closable: false,
    items: [{
        xtype: 'textfield',
        fieldLabel: 'Target: ',
        labelAlign: 'right',
        name: 'username',
        id: 'impersonateModal',
        allowBlank: false,
        margin: '20 10 20 0',
        listeners: {
            specialkey: function (f, e) {
                if (e.getKey() == e.ENTER) {
                    impersonateHandler();
                }
            }
        }
    }],
    buttons: [{
            text: 'Ok',
            handler: function () {
                impersonateHandler();
            }
        },
        {
            text: 'Close',
            handler: function () {
                Ext.getCmp('impersonateModalComponent').hide();
            }
        }
    ]
})
