 
 

 
 						
 	


 

/**
 * The store used for summits
 */
Ext.define('CF.store.ActivityStore', { 
 						  extend:'Ext.data.Store',
   						  requires: [
    							'Ext.grid.*',
							    'Ext.data.*',
							    'Ext.util.*',
							    'Ext.grid.plugin.BufferedRenderer'
   							 		],
   						 
   						  model:   'CF.model.Activity',
   						  alias: 'store.activitystore',
   						  storeId: 'activityStore',
   						   
   						  // allow the grid to interact with the paging scroller by buffering
   					     buffered: true,
   					     leadingBufferZone: 30,
   					     pageSize: 300,
     				     
 					     
				         proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an Ajax proxy would be better
   				         type: 'ajax',
  				          url: '',
  				          reader: {
   						             root: 'graph',
  						             totalProperty: 'totalCount'
  						          },
  						  simpleSortMode: true
    
     					   },
        
     				    
     				   
     				 
 						 
 						   
 						});


