<html>
<head>
  <link rel="shortcut icon" href="data:image/x-icon;" type="image/x-icon">
  <script language="javascript" type="text/javascript"
    src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/ext-theme-classic/ext-theme-classic-all.css" />
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/variables.js">// Injected by dockerfile</script>
    <script src="keycloak.js"></script>
  <script>
    if (KEYCLOAK_ENABLED) {
      var keycloak = new Keycloak("<%=request.getContextPath()%>/html/keycloak.json") || undefined;

      keycloak
        .init({
          onLoad: 'login-required',
          checkLoginIframe: false, // !Important to prevent constant refresh
          enableLogging: false,
          promiseType: 'native'
        })
        .then(function (authenticated) {
          console.log(authenticated ? 'authenticated' : 'not authenticated');
          $.ajaxSetup({
            headers: {
              "Authorization": 'Bearer ' + keycloak.token,
            }
          });
        })
        .catch(function (error) {
          console.log(error);
        });

      // Callback to handle access token expiry             
      keycloak.onTokenExpired = function () {
        keycloak.updateToken(5)
          .then(function (refreshed) {
            if (refreshed) {
              Ext.Ajax.setDefaultHeaders({
                "Authorization": 'Bearer ' + keycloak.token,
              })
              $.ajaxSetup({
                headers: {
                  "Authorization": 'Bearer ' + keycloak.token,
                }
              });
            }
          }).catch(function () {
            console.log('Error refreshing token, redirecting')
            keycloak.clearToken()
            keycloak.logout()
          });
      }

      keycloak.onAuthRefreshSuccess = function () {}
      keycloak.onAuthRefreshError = function () {
        console.log('Error refreshing token, redirecting')
        keycloak.clearToken()
        keycloak.logout()
      }
    }
  </script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/openlayers/2.13.1/OpenLayers.debug.js"></script>
  <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/lib/arbor.js"></script>
  <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/_/graphics.js"></script>
  <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/_/renderer.js"></script>

  <!-- comment these lines when using compiled scripts -->

  <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/lib/ext-all-debug.js"> </script>
  <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath()%>/js/app.js"></script>



  <style>

    #graphwrapper{
      display: flex;
      flex-flow: column;
      height: 100%;
      width: 100%;
    }

    #viewportprovwrapper{
      flex: 1 1 auto;
    }

    .my-legend{
        flex: 0 1 auto;
    }

    .my-legend .legend-title {
      text-align: left;
      margin-bottom: 8px;
      font-weight: bold;
      font-size: 110%;
    }

    .my-legend .legend-scale ul {
      list-style: none;
    }

    .my-legend .legend-scale ul li {
      display: block;
      float: left;
      width: 60px;
      margin-bottom: 6px;
      text-align: center;
      font-size: 100%;
      list-style: none;
    }

    .my-legend ul.legend-labels li span {
      display: block;
      float: left;
      height: 12px;
      width: 60px;
    }

    .my-legend .legend-source {
      font-size: 110%;
      clear: both;
    }

    .my-legend a {
      color: #777;
    }

    #search-results a {
      color: #385F95;
      font: bold 11px tahoma, arial, helvetica, sans-serif;
      text-decoration: none;
    }

    #search-results a:hover {
      text-decoration: underline;
    }

    #search-results p {
      margin: 3px !important;
    }

    .search-item {
      font: normal 11px tahoma, arial, helvetica, sans-serif;
      padding: 3px 10px 3px 10px;
      border: 1px solid #fff;
      border-bottom: 1px solid #eeeeee;
      white-space: normal;
      color: #555;
    }

    .search-item h3 {
      display: block;
      font: inherit;
      font-weight: bold;
      color: #222;
    }

    .search-item h3 span {
      float: right;
      font-weight: normal;
      margin: 0 0 5px 5px;
      width: 100px;
      display: block;
      clear: none;
    }

    .x-form-clear-trigger {
      background-image: url(../../resources/themes/images/default/form/clear-trigger.gif);
    }

    .x-form-search-trigger {
      background-image: url(../../resources/themes/images/default/form/search-trigger.gif);
    }

    .cf-header {
      background-color: #252F49;
      border-bottom: 1px solid #16191D;
      color: white;
      font-size: 16px;
      font-weight: bold;
      padding: 10px;
      text-align: center;
      text-shadow: 0 1px 0 #16191D;
    }

    .cf-helpwindow {
      background-color: #FFFFFF;
      padding: 5px;
    }

    .cf-helpwindow div.cascade {
      padding: 0 0 0 5px;
    }

    .cf-helpwindow h2 {
      margin: 2px 0 0 0;
    }

    .x-tip {
      width: auto !important;
    }

    .x-tip-body {
      width: auto !important;
    }

    .x-tip-body span {
      width: auto !important;
    }
  </style>
</head>

<body>
  <%@ include file="init.jsp" %>

  <div style=" background-color: red; display: none; left: 100px; position: absolute; top: 100px; z-index: 100;">
    The user is: <br><br>
  </div>
</body>

</html>
