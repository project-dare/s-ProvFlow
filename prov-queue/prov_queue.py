from flask import Flask
from flask import request
from flask import Response
from flask_apispec import use_kwargs, doc
from marshmallow import fields
import pika
import os, logging, datetime, json, multiprocessing, http, http.client, urllib, time, sys
import logging.handlers as handlers

app = Flask(__name__)

app.config['DEBUG'] = False

PROVENANCE_QUEUE = "insert_provenance"

sprov_host=os.environ["SPROV_API_HOST"]
sprov_port=int(os.environ["SPROV_API_PORT"])
rabbitmq_host=os.environ["MSGQ_HOST"]
rabbitmq_port=int(os.environ["MSGQ_PORT"])


def main():
    app.logger.info("Provenance queue producer started. PID: %d. sprov API at %s:%s. rabbitmq at %s:%s" %
                    (os.getpid(), sprov_host, sprov_port, rabbitmq_host, rabbitmq_port))
    logging.info("Provenance queue producer started..  PID: %d. sprov API at %s:%s. rabbitmq at %s:%s" %
                 (os.getpid(), sprov_host, sprov_port, rabbitmq_host, rabbitmq_port))
    return app


@app.route("/workflowexecutions/insert", methods=['POST'])
@use_kwargs({'prov': fields.Str(description="JSON document with one of more provenance documents (currently supports the format accepted in the s-ProvFlowMongoDB implementation)")})
@doc(tags=['acquisition'], description='Bulk insert of bundle or lineage documents in JSON format. These must be provided as encoded string in a POST request. The request is queued if a 202 result is returned.')
def insert_provenance(**kwargs):
    payload = kwargs if "prov" in kwargs else request.get_data()
    app.logger.debug("Request headers: %s" % request.headers)
    app.logger.debug("Payload: %s" % payload)
    try:
        auth_token = None
        if "Authorization" in request.headers:
            auth_token = request.headers["Authorization"]
            app.logger.debug("Adding authorization token to provenance document: %s" % auth_token)
        if not publish_message_to_queue(PROVENANCE_QUEUE, payload, app.logger, auth_token):
            app.logger.error("Message queue not available, sending provenance directly.")

        response = Response(json.dumps({"success":True, "inserts":"queued"}), status="202")
        response.headers['Content-type'] = 'application/json'
        app.logger.info("POST insert provenance")
        return response
    except:
        app.logger.exception("insert_provenance: Exception trying to connect to message queue.")

def publish_message_to_queue(mqueue, payload, logger, auth_token=None):
    logger.debug("Insert into queue %s: %s" % (mqueue, payload))
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=rabbitmq_host, port=rabbitmq_port))
        channel = connection.channel()
        channel.queue_declare(queue=mqueue)
        # When retrieved from a queue, the payload might be bytes.
        if not isinstance(payload, dict):
            payload_dict = json.loads(payload)
        else:
            payload_dict = payload

        if auth_token:
            logger.debug("Adding authorization token to payload: %s" % auth_token)
            payload_dict["Authorization-Token"] = auth_token

        ## Re-add the prov key. It's possible that part has single quotes and is not valid JSON, so make sure it is.
        channel.basic_publish(exchange='', routing_key=mqueue, body=str(json.JSONEncoder().encode(payload_dict)))
        connection.close()
        return True
    except Exception as e:
        logger.exception("publish_message_to_queue: Exception trying to connect to message queue.")
        logger.exception(e)
        return False

if __name__ == "__main__":
    main()
