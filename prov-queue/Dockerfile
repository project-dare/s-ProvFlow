# image knmi
FROM ubuntu:xenial

LABEL description="KNMI PROVENANCE QUEUE CONTAINER"

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=UTC

ENV SPROV_API_HOST=sprov
ENV SPROV_API_PORT=8082
ENV MSGQ_HOST=rabbitmq
ENV MSGQ_PORT=5672

RUN mkdir -p /prov-queue/
WORKDIR /prov-queue/

# linux tools
RUN apt-get update && apt-get install -y \
  curl  \
  unzip \
  procps \
  python3 \
  python3-pip \
  python3-dev \
  libxml2-dev \
  libxslt-dev \
  vim

# python requirements
ADD ./requirements.txt ./requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

COPY . .
EXPOSE 5000
RUN touch docker_entry.sh
RUN echo 'python3 ./queue_reader.py &' >> docker_entry.sh
RUN echo 'gunicorn --reload -w 9 -b 0.0.0.0:5000 "prov_queue:main()" --chdir /prov-queue/ --log-level debug --backlog 0 --timeout 120 --error-logfile - --log-file -' >> docker_entry.sh

ENTRYPOINT ["bash", "./docker_entry.sh"]
