from flask import Flask
from flask import request
from flask import Response
from flask_apispec import use_kwargs, doc
from marshmallow import fields
import pika
import os, logging, datetime, json, multiprocessing, http, http.client, urllib, time, sys
import logging.handlers as handlers

from prov_queue import publish_message_to_queue, sprov_host, sprov_port, PROVENANCE_QUEUE

LOG_LEVEL = logging.INFO
LOG_LEVEL_PIKA = logging.ERROR

FALLBACK_QUEUE = "fallback_insert_provenance"

rabbitmq_host=os.environ["MSGQ_HOST"]
rabbitmq_port=int(os.environ["MSGQ_PORT"])


def main():
    reader_process = multiprocessing.Process(target=reader)
    reader_process.start()
    fallback_reader()

def configure_logger(appname, level):
    logger = logging.getLogger(appname)
    if ((not logger.hasHandlers()) or
        (1 == len(logger.handlers) and isinstance(logger.handlers[0], logging.NullHandler))):  # Pika has default a NullHandler
        logger.setLevel(level)
        formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
        # create console handler
        ch = logging.StreamHandler(stream=sys.stdout)
        ch.setLevel(level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)
        # To prevent the "root" channel from being used also
        logger.propagate = False
        logger.info("%s Logger started with handlers %s" % (appname,logger.handlers ))
    return logger


def reader():
    logger = configure_logger("queue-reader", LOG_LEVEL)
    pika_logger = configure_logger("pika", LOG_LEVEL_PIKA)
    logger.info("Provenance queue consumer started. PID: %d. sprov API at %s:%s. rabbitmq at %s:%s" %
                 (os.getpid(), sprov_host, sprov_port, rabbitmq_host, rabbitmq_port))
    while True:
        try:
            logger.debug("Reader process started.")
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host=rabbitmq_host, port=rabbitmq_port))
            channel = connection.channel()
            channel.queue_declare(queue=PROVENANCE_QUEUE)
            channel.basic_consume(queue=PROVENANCE_QUEUE, auto_ack=True, on_message_callback=insert_prov_callback)
            channel.start_consuming()
        except:
            logger.exception("Exception in queue reader process.")
            time.sleep(2)



def fallback_reader():
    PID = os.getpid()
    logger = configure_logger("fb-queue-reader", LOG_LEVEL)
    pika_logger = configure_logger("pika", LOG_LEVEL_PIKA)
    logger.info("Fallback queue consumer started. PID: %d sprov API at %s:%s. rabbitmq at %s:%s" %
                 (PID, sprov_host, sprov_port, rabbitmq_host, rabbitmq_port))
    while True:
        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_host, port=rabbitmq_port))
            channel = connection.channel()
            result = channel.queue_declare(queue=FALLBACK_QUEUE)
            waiting_messages = result.method.message_count

            if (waiting_messages > 0): logger.info("Going to process %d available messages in fallback_queue before going to sleep again" % (waiting_messages))
            for n in range(waiting_messages):
                method, properties, body = channel.basic_get(FALLBACK_QUEUE, auto_ack=False)
                logger.debug("Publish message from fallback_queue to insert_queue: %s" % (body))
                if not publish_message_to_queue(PROVENANCE_QUEUE, body.decode("UTF-8"), logger):
                    break
                channel.basic_ack(delivery_tag=method.delivery_tag, multiple=False)

            channel.cancel()
            channel.close()
            connection.close()

            logger.info("Going to sleep")
            time.sleep(30)

        except:
            logger.exception("Exception in queue fallback_reader process.")
            time.sleep(2)

def insert_prov_callback(ch, method, properties, body):
    connection = None
    logger = configure_logger("insert_prov_callback", LOG_LEVEL)
    pika_logger = configure_logger("pika", LOG_LEVEL_PIKA)
    try:
        logger.debug("Insert CB Received: %s" % body.decode("UTF-8"))
        prov = json.loads(body.decode("UTF-8"))
        params = urllib.parse.urlencode(prov)
        headers = {"Content-type": "application/x-www-form-urlencoded",
                   "Accept": "application/json"}

        auth_token=None
        if "Authorization-Token" in prov:
            logger.debug("Extracting authorization header from payload: %s" % prov["Authorization-Token"])
            headers["Authorization"] = prov["Authorization-Token"]
            auth_token = prov["Authorization-Token"]
            del prov["Authorization-Token"]
        try:
            connection = http.client.HTTPConnection(os.environ["SPROV_API_HOST"], port=int(os.environ["SPROV_API_PORT"]))
            connection.request("POST", "/workflowexecutions/insert", params, headers)
        except Exception as e:
            logger.info("Connection to SPROV_API failed, inserting message to fallback queue")
            publish_message_to_queue(FALLBACK_QUEUE, prov, logger, auth_token=auth_token)  # No connection to SPROV-API and FB queue? message lost!
            return

        api_response = connection.getresponse()
        #TODO: Consider check on response status <> 200. Put message to fallback-queue
        logger.info("Response from API on insert: %d: %s" % (api_response.status, api_response.reason))
        return api_response
    except Exception as e:
        logger.exception(e)
    finally:
        if connection: connection.close()

main()
