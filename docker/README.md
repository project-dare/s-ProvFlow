# Disabling authentication for development
If you need to run the docker-compose without authentication, you can use the docker-compose-noauth file:
```
docker-compose -f docker-compose-noauth.yml up --build
```
or to enable automatic reloading of the API code when you make changes:
```
docker-compose -f docker-compose-noauth.yml -f docker-compose.dev.yml up --build
```

# Authentication setup
The docker compose file will set up authentication proxies in front of sprov and sprov viewer.
It also deploys a preconfigured Keycloak environment with a realm deployed for sprov.
The realm contains two clients:
- sprovflow -- The client representing the sprov backend
- dispel4py -- The client representing dispel4py, which is allowed to use the sprov backend. (deprecated)

The realm contains one user with username test and password test. This user has access to the sprovflow backend.

## Overview of services
The image below shows all the services started by the docker-compose file and the public paths 
via which they can be reached.

![Overview of services](docker-compose-overview.png)

The original image is stored in Google drive: https://drive.google.com/file/d/1kZl492va5jvVru8ZhzsLOJukoFE6QTdm

**Note:**
The Keycloak gatekeepers access keycloak via the internal docker network, 
whilst the user authenticates with keycloak via the host network. 
Keycloak will only accept tokens which are issued by the same host. 
To make sure that both on the internal docker network and on the host network the keycloak hostname is equal, 
the following actions have been taken:
1. Traefik exposes keycloak via the host keycloak.localhost on port 8080. 
2. Via Docker compose the service name of keycloak is keycloak.localhost. 
Other docker containers can reach the service on the internal docker network using the service name as the hostname.


## Using dispel4py with authentication
The environment comes deployed with a preconfigured dispel4py client which can be used when authenticating dispel4py to sprov.
Dispel4py will need to set the following command line arguments:
```
--provenance-userid=<provenance-userid>
--provenance-bearer-token="<valid base64 encoded JWT>"
```

You can create a token with:
```shell script
validtoken=$(curl --silent -X POST http://keycloak.localhost:8080/auth/realms/sprov/protocol/openid-connect/token \
  -H "Content-Type: application/x-www-form-urlencoded" -d "username=test" -d "password=test" \
  -d 'grant_type=password' -d "client_id=dispel4py" -d "client_secret=b4f8c98a-0604-465c-b9e9-942644885a76" \
  | jq -r '.access_token')
```
And then call dispel4py with:
```shell script
python -m dispel4py.new.processor -f dispel4py/examples/prov_testing/input-mysplitmerge \
  --provenance-config=dispel4py/examples/prov_testing/prov-config-mysplitmerge.json \
  --provenance-repository-url="http://localhost/prov/workflowexecutions/insert" \
  --provenance-userid=<provenance-userid> --provenance-bearer-token="${validtoken}" \
  simple dispel4py/examples/prov_testing/mySplitMerge_prov.py
```
Then you will find the inserted provenance in the viewer. If you wait for some time the token will become invalid
(see keycloak config for the exact time) and if you then run dispel4py again with the same token, there will be no
provenance stored because the queue is not authorized to call the `/workflowexecutions/insert` end point on the API.

## Exporting Keycloak environment
To ensure that a keycloak dev environment is configured, the keycloak container is started with an import
of the sprov realm. To update the default configuration, you will need to create a new export:
```
docker exec -it keycloak.localhost /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.realmName=sprov -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/sprov.json
```
Then copy the file to your host system:
```
docker cp keycloak.localhost:/tmp/sprov.json .
```
And move the contents of the file to `keycloak-sprov-dev-realm.json`
The new configuration will be automatically loaded when starting the docker-compose file.

Note that the exported development keycloak config contains hardcoded client-secrets and a hardcoded user with username test and password test.

## Issue with audience and client-id
Gatekeeper has a bug where it requires the client-id to be in the audience:
https://issues.redhat.com/browse/KEYCLOAK-8954
In this setup we worked around this issue by adding a mapper to the sprov client which maps client-id as audience.

## Authentication for sprovflow-viewer
There is an issue with stripping the /prov path and authentication redirect for gaining token via browser.
Solved this by adding an extra gatekeeper in front of the frontend.
Currently the frontend and the backend gatekeeper use the same client id,
such that the backend can also use refresh token, since frontend does not refresh automatically.
In the future we need to update the frontend such that it can silently refresh the token.


 
