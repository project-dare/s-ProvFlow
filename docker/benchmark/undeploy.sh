#!/bin/bash

keypairname=DEV_KNMI_DARE_SPROVFLOW_BENCHMARK
stackname=dare-sprovflow-benchmark-eu-west-1


echo -n "Removing EC2 instances. "
aws cloudformation delete-stack --stack-name ${stackname}
echo "Done."

keypairid=$(aws ec2 describe-key-pairs --key-names ${keypairname} 2> /dev/null | jq -r .KeyPairs[0].KeyPairId)
if [ ! -z ${keypairid} ] ; then
    echo -n "Removing keypair ${keypairname} with ID ${keypairid}. "
    aws ec2 delete-key-pair --key-name ${keypairname}
    rm -f ${keypairname}.pem
    echo "Done."
fi
