#!/bin/bash
if [ -z ${AWS_PROFILE} ] ; then
    echo "Need to authenticate first or set AWS_PROFILE environment variable."
    exit 1
fi

keypairname=DEV_KNMI_DARE_SPROVFLOW_BENCHMARK
stackname=dare-sprovflow-benchmark-eu-west-1

keypairid=$(aws ec2 describe-key-pairs --key-names ${keypairname} 2> /dev/null | jq -r .KeyPairs[0].KeyPairId)
if [ -z ${keypairid} ] ; then
    echo -n "Create SSH key pair. "
    [ -e ${keypairname}.pem ] && mv ${keypairname}.pem BACKUP-${keypairname}.pem.$(date '+%Y%m%d%H%M%S')
    aws ec2 create-key-pair --key-name ${keypairname} | jq -r .KeyMaterial > ${keypairname}.pem
    chmod 600 ${keypairname}.pem
    echo "Created key pair ${keypairname}."
fi

echo -n "Deploy EC2 instances. "
aws cloudformation deploy --stack-name ${stackname} \
    --capabilities CAPABILITY_NAMED_IAM --template-file benchmark.cf.yml \
    --parameter-overrides SSHKeyPairName=DEV_KNMI_DARE_SPROVFLOW_BENCHMARK
echo "Done."

## Determine DNS names of the nodes.
svc_node_id=$(aws cloudformation describe-stacks --stack-name ${stackname} \
    --query 'Stacks[0].Outputs[?OutputKey==`SProvFlowServiceBenchmarkInstanceName`].OutputValue' --output text)
wf_node_id=$(aws cloudformation describe-stacks --stack-name ${stackname} \
    --query 'Stacks[0].Outputs[?OutputKey==`SProvFlowWorkflowBenchmarkInstanceName`].OutputValue' --output text)
svc_node_dns=$(aws ec2 describe-instances --instance-ids ${svc_node_id} \
    --query 'Reservations[].Instances[].PublicDnsName' --output text)
wf_node_dns=$(aws ec2 describe-instances --instance-ids ${wf_node_id} \
    --query 'Reservations[].Instances[].PublicDnsName' --output text)

echo "Service node DNS: ${svc_node_dns}"
echo "Workflow node DNS: ${wf_node_dns}"
