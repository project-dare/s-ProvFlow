#/usr/bin/env python3

import glob, sys, os, time
import http.client
import urllib.parse
import ujson

def main():

    if len(sys.argv) < 2:
        endpoint = os.environ['REPOS_URL']
    else:
        endpoint = sys.argv[1]
    endpoint_parsed = urllib.parse.urlparse(endpoint)
    endpoint_host = endpoint_parsed.netloc
    endpoint_path = endpoint_parsed.path
    total_response_time = 0.0
    nr_requests = 0
    provpath=os.environ['PROV_PATH']
    provfiles = glob.glob('%s/bulk*' % provpath)
    for provfilename in provfiles:
        with open(provfilename, "r") as provfile:
            provdoc = provfile.read()
            params = urllib.parse.urlencode({'prov': ujson.dumps(provdoc)})
            headers = {
                "Content-type": "application/x-www-form-urlencoded",
                "Accept": "application/json"}
            conn = http.client.HTTPConnection(endpoint_host)
            starttime = time.time()
            conn.request("POST", endpoint_path, params, headers)
            response = conn.getresponse()
            endtime = time.time()
            total_response_time = total_response_time + (endtime-starttime)
            nr_requests += 1
            print("Response for %s: %s" % (provfilename, response.code))
    print("==================")
    print("TOTAL Response time:", total_response_time)
    print("AVG Response time %s requests: %s" % (nr_requests, total_response_time/nr_requests))
    return 0

if __name__ == "__main__":
    main()
