# Running s-ProvFlow API bench mark

The purpose of this is to compare the difference in overhead imposed
on services using the s-ProvFlow API with the provenance insert queue
enabled or disabled.

## Deployment

You need a valid AWS user account in the KNMI RDWD department's AWS
account to deploy. When authenticated deploying the two EC2 compute
nodes is a matter of running:

```
./deploy.sh
```

A SSH key is automatically generated and stored with mode 600 in the
current working directory. The DNS names of the service node and the
workflow node will be logged when the cloudformation stack has been
deployed. Please note it will take some time before the nodes are fully
initialized even after the script returns. You can login with:

```
ssh -i DEV_KNMI_DARE_SPROVFLOW_BENCHMARK.pem ec2-user@<node-dns-name>
```

You can see if the initialization is done by logging in and checking the
`/var/log/cloud-init-output.log` on the machine:
```
tail -f /var/log/cloud-init-output.log
```

### Undeploying

To take everything down type `./undeploy`. The stack and the SSH key
will be deleted.

## Running the workflow.

Before running the workflow on the workflow node, you must make sure the
URL is correctly set in
`~/WP6_EPOS/processing_elements/Download_Specfem3d_Misfit_RA/run_total_combined.sh`.
Change the line:

```
export REPOS_URL="http://testbed.project-dare.eu/prov/workflowexecutions/insert"
```
to point to the DNS name of the service node logged by the `deploy.sh` script.

Then you can run the workflow with:
```
cd ~/WP6_EPOS/processing_elements/Download_Specfem3d_Misfit_RA/
./run_total_combined.sh
```

## Checking the provenance in the viewer

You can check the inserted provenance by pointing your web browser to
http://<dns name of service node>/sprovflow-viewer/html/view.jsp and
supplying `fmagnoni` as the userid in the dialog.

## Checking s-ProvFlow logs

Login to the service node with SSH and type `docker logs -f <service-name>`.
For instance, to see the logs of the `prov-queue` service type:
`docker logs -f <service-name>`. To see all services:
```
cd ~/s-ProvFlow/docker/benchmark
docker-compose -f docker-compose-noauth.yml ps
```

## Benchmark results:

Command used: `/usr/bin/time ./run_total_combined.sh`

### Workflow total time

Service with Queue enabled (commit 9030158a2dab1b841ee16b61584f305bf8a253ae):
> Reported by dispel4py: ELAPSED TIME: 100.82056498527527
>
> Reported by `/usr/bin/time` command:
> 104.62user 6.76system 2:05.33elapsed 88%CPU (0avgtext+0avgdata 317720maxresident)k
> 0inputs+9552outputs (0major+279467minor)pagefaults 0swaps

Stored to file:
> Reported by dispel4py: ELAPSED TIME: 102.99777483940125
>
> Reported by `/usr/bin/time` command:
> 101.39user 6.14system 1:58.87elapsed 90%CPU (0avgtext+0avgdata 316960maxresident)k
> 0inputs+50472outputs (0major+291499minor)pagefaults 0swaps

Service with Queue disabled (commit 0f2daf023b466b5d7740ce8f6c2dd65130eb9c25
with provenance-api/requirements.txt from commit 9030158a2dab1b841ee16b61584f305bf8a253ae):
> Reported by dispel4py: ELAPSED TIME: 103.03054523468018
>
> Reported by `/usr/bin/time` command:
> 104.05user 6.46system 2:04.84elapsed 88%CPU (0avgtext+0avgdata 317076maxresident)k
> 0inputs+9528outputs (0major+279879minor)pagefaults 0swaps


### Storing provenance with API call after run

Running one request at a time.
Script that was run:
```shell script
eval `grep REPOS_URL run_total_combined.sh` # Set REPOS_URL env var.
export PROV_PATH=/home/ec2-user/WP6_EPOS/processing_elements/Download_Specfem3d_Misfit_RA/prov_data
python s-ProvFlow/docker/benchmark/insertbulk.py
```

Alternative (not tested):
```shell script
cd prov_data
mkdir ../prov_data_enveloped
for provfile in `ls` ; do
  cat > ../prov_data_enveloped/${provfile} <<EOF
{"prov":`cat ${provfile}`}
EOF
done
cd ../prov_data_enveloped
starttime=`date '+%s'`
eval `grep REPOS_URL run_total_combined.sh` # Set REPOS_URL env var.
for provfile in `ls` ; do
    ab -c 1 -n 1 -T "application/json" -p ${provfile} ${REPOS_URL}
done
stoptime=`date '+%s'`
echo "Running time:" $((stoptime - starttime))
```

With Queue disabled:
> TOTAL Response time: 4.118862628936768
> AVG Response time 1124 requests: 0.0036644685310825336

With Queue enabled:
> TOTAL Response time: 10.98364782333374
> AVG Response time 1124 requests: 0.009771928668446388

## Benchmark results after gunicorn

Service with Queue disabled (commit 0f2daf023b466b5d7740ce8f6c2dd65130eb9c25
with provenance-api/requirements.txt from commit 9030158a2dab1b841ee16b61584f305bf8a253ae
and provenance-service/Dockerfile changes from commit 7cb92e46b080c2568400d1e12cbd28f3dd3d01f1
to get around the timezone issue when building the sprov docker image):
> Reported by dispel4py: ELAPSED TIME: 100.57595944404602
>
> Reported by `/usr/bin/time` command:
> 105.02user 6.31system 2:03.55elapsed 90%CPU (0avgtext+0avgdata 316760maxresident)k
> 0inputs+9528outputs (0major+292330minor)pagefaults 0swaps

Service with Queue enabled:
> Reported by dispel4py: ELAPSED TIME: 105.85134506225586
> Reported by `/usr/bin/time` command:
> 104.71user 6.50system 2:10.09elapsed 85%CPU (0avgtext+0avgdata 317168maxresident)k
> 0inputs+9528outputs (0major+292218minor)pagefaults 0swaps

Strange result. The dispel4py reported time is higher, but the `time` command reports a similar
time. The only thing left to try is to change the code substantially so that we only have one
reader process.

### Using insertbulk after run.

With queue enabled:
> TOTAL Response time: 57.56071066856384
> AVG Response time 5620 requests: 0.010242119336043388

So again this is comparable to not using gunicorn.

### Separating the reader process from the queue's API end point process.

When running the `prov-queue` as is from gunicorn there are a large number
of processes, because each process spawned by gunicorn forks two child
processes, one for the reader and one for the fallback reader. Separating
these, so we have multiple spawned API end point processes, but only one
reader and only one fallback reader. (Commit: 37cf7163bc9dcb40ad920a2ee3b9f3656c50a9cc)

>  by dispel4py: ELAPSED TIME: 105.28063416481018
>
> Reported by `/usr/bin/time` command:
> 104.11user 6.36system 2:10.17elapsed 84%CPU (0avgtext+0avgdata 316732maxresident)k
> 0inputs+9528outputs (0major+284762minor)pagefaults 0swaps

So no difference between the multiple reader process and the single reader
process here. Use insertbulk:
> TOTAL Response time: 66.86324644088745
> AVG Response time 5620 requests: 0.01189737481154581

If anything, this is even slower.
