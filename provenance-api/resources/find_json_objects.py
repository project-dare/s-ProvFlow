#!/usr/bin/python3
# ***********************************************************************
# *                      All rights reserved                           **
# *                     Copyright (c) 2020 KNMI                        **
# *             Royal Netherlands Meteorological Institute             **
# ***********************************************************************
# * Function   : Find paths to and print objects containing a Key or Value with 'value' in 'input' json file
# * Purpose    : 
# * Usage      : ./find_json_objects.py <json file> <value>
# *
# * Project    : DARE
# *
# * initial programmer :  Hans Verhoef
# * initial date       :  20200127
# **********************************************************************

import json
import argparse

verbose = False

def find_values(value, dictionary, path=""): 
    if verbose: print ("looking for %s in path %s" % (value, path)) 
    for k, v in dictionary.items(): 
        if verbose:print('inspecting %s : %s' %( k, type(v))) 
        if v == value:
            if verbose: print ("Found value in path %s" % path) 
            yield path, dictionary
        elif k == value:
            if verbose: print ("Found value as key in path %s" % path)
            yield path + "['" + k + "']", dictionary[k]
        elif isinstance(v, dict): 
            for result in find_values(value, v, path = path + "['" + k + "']"): 
                yield result
        elif isinstance(v, list):
            n = 0
            for d in v:
                for result in find_values(value, d, "{0}['{1}'][{2:d}]".format(path, k, n)):
                    yield result
                n += 1  
    
def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Find paths to and print objects containing a Key or Value with 'value' in 'input' json file")
    parser.add_argument("input", help="input json file ")
    parser.add_argument("value", help="value to look for")
    parser.add_argument("--verbose", dest="verbose", action="store_true",
                        required=False, default=False,
                        help="Show verbose logging on stdout.")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    verbose = args.verbose
    with open(args.input, "r") as read_file:
        json_dict=json.load(read_file)
    for path, dictionary in find_values(args.value, json_dict):
        print("=============================")
        print(path)
        print(json.dumps(dictionary, indent=4))