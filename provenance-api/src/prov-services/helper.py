import numbers
import copy
import datetime

def addIndexedContentToLineage(lineage):
    MAXIMUM_STRING_SIZE_FOR_INDEXING = 50
    lineage_updated = copy.deepcopy(lineage)

    if 'streams' in lineage_updated and type(lineage_updated['streams']) == list:
        for stream in lineage_updated['streams']:
            if 'content' in stream and type(stream['content']) == list:
                all_content_map = {}
                for content in stream['content']:
                    if type(content) == dict:
                        for key in content:
                            if isinstance(content[key], numbers.Number) or ( type(content[key]) is str and len(content[key]) < MAXIMUM_STRING_SIZE_FOR_INDEXING):
                                if key not in all_content_map:
                                    all_content_map[key] = {}
                                    all_content_map[key][content[key]] = 1
                                else:
                                    all_content_map[key][content[key]] = 1

                indexedMeta = []
                for map_key in all_content_map:
                    for map_value in all_content_map[map_key]:
                        indexedMeta.append({
                            'key': map_key,
                            'val': map_value
                        })
                stream['indexedMeta'] = indexedMeta

    if 'parameters' in lineage_updated and type(lineage_updated['parameters']) == dict:
        parametersKeyVal = []
        for key in lineage_updated['parameters']:
            parametersKeyVal.append({
                'key': key,
                'val': lineage_updated['parameters'][key]
                })
        lineage_updated['parameters'] = parametersKeyVal

    lineage_updated['insertedAt'] = datetime.datetime.now()

    return lineage_updated

def lineageToJsonLd(lineage):
    jsonLd = {}
    return jsonLd

def jsonLdToLineage():
    lineage = {}
    # TODO implement transformation of JSON-LD to lineage
    return lineage

def workflowToJsonLd(lineage):
    jsonLd = {}
    # TODO implement transformation of workflow to JSON-LD
    return jsonLd

def jsonLdToWorkflow():
    workflow = {}
    # TODO implement transformation of JSON-LD to workflow
    return workflow

def getIndexedMetaQueryList(KeyValuePairs, optionalFormat=None):
    indexedMetaQueryList = []
    for key_value_pair in KeyValuePairs:
        item = {
            'streams': {
                '$elemMatch': {
                    'indexedMeta': {
                        '$elemMatch': key_value_pair
                    }
                }
            }
        }
        if optionalFormat is not None:
            item['streams']['$elemMatch']['format'] = optionalFormat

        indexedMetaQueryList.append(item)
    return indexedMetaQueryList

def getParametersQueryList(KeyValuePairs):
    parametersQueryList = []
    for key_value_pair in KeyValuePairs:
        parametersQueryList.append({
            'parameters': {
                '$elemMatch': key_value_pair
            }
        })
    return parametersQueryList

def getAndQueryList(KeyValuePairs):
    parametersQueryList = []
    for key_value_pair in KeyValuePairs:
        parametersQueryList.append({
            'indexedMeta': {
                '$elemMatch': key_value_pair
            }
        })
    return parametersQueryList

def getAndQueryIndexedMetaAndParameters(KeyValuePairs):
    query = []
    for key_value_pair in KeyValuePairs:
        query.append({
            '$or': [
                {
                    'indexedMeta': {
                        '$elemMatch': key_value_pair
                    }
                },
                {
                    'parameters': {
                        '$elemMatch': key_value_pair
                    }
                }
            ]
        })
    return query


def getUnwindedStreamIndexedMetaQuery(KeyValuePairs, optionalFormat=None):
    parametersQueryList = []
    for key_value_pair in KeyValuePairs:
        item = {
            'streams.indexedMeta': {
                '$elemMatch': key_value_pair
            }
        }
        if optionalFormat != None:
            item['streams.format'] = optionalFormat
        parametersQueryList.append(item)
    return parametersQueryList

# MV: 10/2020 This method seems to be unused:
# def getKeyValuePairs(keylist, maxvalues, minvalues):
#     keys = copy.deepcopy(keylist)
#     maxValList = copy.deepcopy(maxvalues)
#     minValList = copy.deepcopy(minvalues)

#     try:
#         key_val_match_list = []

#         for key in keys:
            
#             maxval=num(maxValList.pop(0))
#             minval=num(minValList.pop(0))

#             value = {
#                 '$gte': minval,
#                 '$lte': maxval
#             }

#             if maxval == minval:
#                 value = maxval

#             key_val_match_list.append({
#                 'key': key,
#                 'val': value
#             })

#         return key_val_match_list
#     except ValueError:
#         # TODO how to handle error?
#         return []

def num(s):
    try:
        return float(s)
    except:
        return str(s)

def parse_values(s):
    '''
    Parse numerical values from string.
    '''
    try:
        return float(s) if '.' in s else int(s)
    except:
        return str(s)
     
def getKeyValuePairsExtended(keylist,vals):
    '''
    Modified version of getKeyValuePairs that expects inputs from a single
    text field per term. Inputs can be:
    - A list: 1,2,3 or a,b,c
    - A range: 1...3 or a...z
    - A specific term, e.g. a boolean  
    '''
    keys = copy.deepcopy(keylist)
    value_collection = copy.deepcopy(vals)

    try:
        key_val_match_list = []

        for key in keys:
            field_value = value_collection.pop(0)

            if "..." in field_value:
                value = {}
                minval, maxval = field_value.split('...')

                if minval :
                    value['$gte'] = parse_values(minval)
                if maxval:
                    value['$lte'] = parse_values(maxval) 

            elif "%2C" in field_value:
                value_list = field_value.replace("%2C", ",").split(",")

                if value_list:
                    try:
                        value_list = list(map(parse_values, value_list))
                    except ValueError:
                        pass

                value = {
                    '$in': value_list
                }
            
            else:
                if(field_value == 'true'):
                    target_value = True
                elif(field_value == 'false'):
                    target_value = False
                else:
                    target_value = parse_values(field_value)

                value = {
                    '$eq': target_value
                }

            key_val_match_list.append({
                'key': key,
                'val': value
            })

        return key_val_match_list
    except ValueError:
        # TODO how to handle error?
        return []
