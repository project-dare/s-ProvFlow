import provenance as provenance
import cwlprov_2_sprov as cwlprov_2_sprov
import json
import webargs
import csv
from io import StringIO
import traceback
import datetime
from prov.model import ProvDocument, Namespace, Literal, PROV, Identifier
from flask import Flask, g
from flask import request, jsonify
from flask import Response
from flask import abort
from flask_cors import CORS
import shutil
import uuid
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
import zipfile

import logging
import sys
import os

from flask_apispec import use_kwargs, marshal_with, doc, FlaskApiSpec
from flask._compat import reraise, string_types, text_type, integer_types
from flask.helpers import _PackageBoundObject, url_for, get_flashed_messages, \
     locked_cached_property, _endpoint_from_view_func, find_package, \
     get_debug_flag
from marshmallow import fields, Schema


#Redefined add_url_rule to disable default generation of OPTION method handler

def add_url_rule_no_options(self, rule, endpoint=None, view_func=None, **options):
    """Connects a URL rule.  Works exactly like the :meth:`route`
    decorator.  If a view_func is provided it will be registered with the
    endpoint.

    Basically this example::

        @app.route('/')
        def index():
            pass

    Is equivalent to the following::

        def index():
            pass
        app.add_url_rule('/', 'index', index)

    If the view_func is not provided you will need to connect the endpoint
    to a view function like so::

        app.view_functions['index'] = index

    Internally :meth:`route` invokes :meth:`add_url_rule` so if you want
    to customize the behavior via subclassing you only need to change
    this method.

    For more information refer to :ref:`url-route-registrations`.

    .. versionchanged:: 0.2
       `view_func` parameter added.

    .. versionchanged:: 0.6
       ``OPTIONS`` is added automatically as method.

    :param rule: the URL rule as string
    :param endpoint: the endpoint for the registered URL rule.  Flask
                     itself assumes the name of the view function as
                     endpoint
    :param view_func: the function to call when serving a request to the
                      provided endpoint
    :param options: the options to be forwarded to the underlying
                    :class:`~werkzeug.routing.Rule` object.  A change
                    to Werkzeug is handling of method options.  methods
                    is a list of methods this rule should be limited
                    to (``GET``, ``POST`` etc.).  By default a rule
                    just listens for ``GET`` (and implicitly ``HEAD``).
                    Starting with Flask 0.6, ``OPTIONS`` is implicitly
                    added and handled by the standard request handling.
    """
    if endpoint is None:
        endpoint = _endpoint_from_view_func(view_func)
    options['endpoint'] = endpoint
    methods = options.pop('methods', None)

    # if the methods are not given and the view_func object knows its
    # methods we can use that instead.  If neither exists, we go with
    # a tuple of only ``GET`` as default.
    if methods is None:
        methods = getattr(view_func, 'methods', None) or ('GET',)
    if isinstance(methods, string_types):
        raise TypeError('Allowed methods have to be iterables of strings, '
                        'for example: @app.route(..., methods=["POST"])')
    methods = set(item.upper() for item in methods)

    # Methods that should always be added
    required_methods = set(getattr(view_func, 'required_methods', ()))

    # starting with Flask 0.8 the view_func object can disable and
    # force-enable the automatic options handling.
    provide_automatic_options = getattr(view_func,
        'provide_automatic_options', None)

    if provide_automatic_options is None:
        if 'OPTIONS' not in methods:
            provide_automatic_options = False

        else:
            provide_automatic_options = True
            required_methods.add('OPTIONS')

    # Add the required methods now.
    methods |= required_methods

    rule = self.url_rule_class(rule, methods=methods, **options)
    rule.provide_automatic_options = provide_automatic_options

    self.url_map.add(rule)
    if view_func is not None:
        old_func = self.view_functions.get(endpoint)
        if old_func is not None and old_func != view_func:
            raise AssertionError('View function mapping is overwriting an '
                                 'existing endpoint function: %s' % endpoint)
        self.view_functions[endpoint] = view_func





Flask.add_url_rule=add_url_rule_no_options

app = Flask(__name__)
app.config['DEBUG'] = False
CORS(app)

# Setup the swagger documentation generation
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin

file_plugin = MarshmallowPlugin() # Plugin to handle upload of files via Marshmallow.

app.config.update({
    'APISPEC_SPEC': APISpec(
        title='s-prov',
        version='v1',
        openapi_version="3.0.2",
        plugins=['apispec.ext.marshmallow', file_plugin],
        schemes=['http','https'],
        description="S-ProvFlow provenance API - Provenance framework for storage and access of data-intensive streaming lineage. It offers a a web API and a range of dedicated visualisation tools and a provenance model (S-PROV) which utilises and extends PROV and ProvONE model"

    ),
    'APISPEC_SWAGGER_URL': '/swagger/',
})

auth_validate_token = False
if "AUTH_VALIDATE_BEARER_TOKEN" in os.environ and os.environ["AUTH_VALIDATE_BEARER_TOKEN"] == "true":
    auth_validate_token = True
    from flask_oidc import OpenIDConnect
    app.config.update({
        'OIDC_CLIENT_SECRETS': 'client_secrets.json',
        'OIDC_OPENID_REALM': 'sprov',
        'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
        'OIDC_TOKEN_TYPE_HINT': 'access_token',
        'OIDC_RESOURCE_SERVER_ONLY': True
    })
else:
    ## Provide a mock decorator
    class OpenIDConnect:
        def __init__(self, app):
            self.app = app
        def accept_token(self, require_token=False):
            def wrapper(func):
                print(app, "Mock decorator: Token required = ", require_token)
                return func
            return wrapper
oidc = OpenIDConnect(app)

@file_plugin.map_to_openapi_type('file', None)
class FileField(fields.Raw):
    pass

def bootstrap_app():
    app.db = provenance.ProvenanceStore(
            os.environ['RAAS_REPO'])  # = mongodb://mongo-db/sprov-db
    return app

@app.route("/")
def hello():
    return "This is the s-prov service"

# Thomas
# Insert sequences of provenance documents, these can be bundles or lineage. The documents can be in JSON or JSON-LD. Format adaptation for storage purposes is handled by the acquisition function.
@app.route("/workflowexecutions/insert", methods=['POST'])
@use_kwargs({'prov': fields.Str(description="JSON document with one of more provenance documents (currently supports the format accepted in the s-ProvFlowMongoDB implementation)")})
@doc(tags=['acquisition'], description='Bulk insert of bundle or lineage documents in JSON format. These must be provided as encoded stirng in a POST request')
@oidc.accept_token(require_token=True)
def insert_provenance(**kwargs):
        app.logger.debug("Request headers: %s" % request.headers)
        payload = kwargs["prov"] if "prov" in kwargs else request.get_data()
        payload = json.loads(str(payload))
        response = Response(json.dumps(app.db.insertData(payload)))
        response.headers['Content-type'] = 'application/json'
        if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":POST insert provenance  - "+" PID:"+str(os.getpid()))
        return response

FORMAT_OPTIONS = ['CWLProv']
@app.route("/workflowexecutions/import", methods=['POST'])
@use_kwargs({
    'format': fields.Str(
        missing='CWLProv',
        enum=FORMAT_OPTIONS,
        description="Format of the provenance output to be imported."),
    'resultLocation': fields.Str(
        required=True,
        description="The root location where the result of the workflow is written."),
    "archive": FileField(
        required=True,
        description="Zip archive of provenance output, which will be mapped to s-ProvFlowMongoDB and stored. "
                    "Currently only files in the CWLProv format are supported"),
    "runId": fields.Str(
        missing=None,
        description="Optional runId that will be used in the provenance document.")
    },
    locations=["form", "files"])
@doc(
    tags=['acquisition'],
    description='Import of provenance output which is not yet mapped to the s-ProvFlowMongoDB format. '
                'The files provided in the archive will be mapped to s-ProvFlowMongoDB if they are in one of the '
                'supported formats.',
    consumes=['multipart/form-data'],
     )
def import_provenance(archive, format, resultLocation, runId):
    if format not in FORMAT_OPTIONS:
        abort(400, 'Format ' + format + ' is not supported.')
    if not isinstance(archive, FileStorage) or os.path.splitext(archive.filename)[1] != '.zip':
        abort(400, 'Archive should be a zip file.')
    try:
        # Save and extract the archive.
        unique_filename = str(uuid.uuid4()) + '-' + secure_filename(archive.filename)
        unique_filepath = os.path.join("", unique_filename)
        archive.save(unique_filepath)

        unique_workdir = os.path.splitext(unique_filepath)[0]
        unique_extract_path= os.path.join(unique_workdir, "input")

        with zipfile.ZipFile(unique_filepath,"r") as zip:
            zip.extractall(unique_extract_path)

        # Perform the mapping, currently only cwlprov_2_sprov is possible.
        unique_output_path= os.path.join(unique_workdir, "output")
        os.mkdir(unique_output_path)
        cwl_runid = cwlprov_2_sprov.cwlprov_2_sprov(unique_extract_path, unique_output_path, resultLocation, runId=runId)
        if cwl_runid and (app.db.workflowExists(cwl_runid) or app.db.workflowExists(runId)):
            result = response={"success":False}
            result.update({"error":"Duplicate workflow."})
        elif cwl_runid:
            # Insert the output of the mapping into s-ProvFLowMongoDB
            result = []
            for file in os.listdir(unique_output_path):
                with open(os.path.join(unique_output_path, file)) as f:
                    data = json.load(f)
                    result.append(app.db.insertData(data))
        else:
            result = response={"success":False}
            result.update({"error":"Could not read provenance file."})

    except Exception as e:
        app.logger.exception(e)
        abort(500, 'Failed to perform the mapping to s-ProvFlowMongoDB due to exception: ' + str(e))
    finally:
        os.remove(unique_filepath)
        shutil.rmtree(unique_workdir)

    response = Response(json.dumps(result))
    response.headers['Content-type'] = 'application/json'
    if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":POST import provenance  - "+" PID:"+str(os.getpid()));
    return response

#Update of the user's description of a provenance bundle document. This allow users to explore and improve the description of a run depending from their findings.
@app.route("/workflowexecutions/<runid>/edit", methods=['POST'])
@use_kwargs({'doc': fields.Str(required=True,description="json document with a description property with the updated text")})
@doc(tags=['acquisition'], description='Update of the description of a workflow execution. Users can improve this information in free-tex')
def wfexec_description_edit(runid,**kwargs):

        #if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":POST WorkflowRunInfo - "+runid);
        payload = kwargs["doc"]
        response = Response(json.dumps(app.db.editRun(runid,json.loads(str(payload)))))
        response.headers['Content-type'] = 'application/json'
        return response

#Deletes the bundle and all the lineage documents related to the \emph{run\_id}.

@app.route("/workflowexecutions/<runid>/delete", methods=['POST'])
@doc(tags=['acquisition'], description='Delete a workflow execution trace, including its bundle and all its lineage documents')
def delete_workflow_run(runid):

        if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":POST workflowexecution delete - "+runid+" PID:"+str(os.getpid()));
        response = Response(json.dumps(app.db.deleteRun(runid)))
        response.headers['Content-type'] = 'application/json'
        return response


#Extract documents from the bundle collection by the \id{run\_id} of a \emph{WFExecution}


paging ={ 'start': fields.Int(required=True,description="index of the starting item"),
          'limit': fields.Int(required=True,description="max number of items expected")
          }


queryargsbasic = {
             'terms': fields.Str(description="csv list of metadata or parameter terms. These relate positionally to the expressions"),
             'expressions': fields.Str(description="csv list of metadata or parameters expressions. These relate positionally to the terms"),
             'wasAssociatedWith': fields.Str(description="csv list of Components involved in the Workflow's Execution"),
             'mode': fields.Str(description="execution mode of the workflow in case it support different kind of concrete mappings (eg. mpi, simple, multiprocess, etc.."),
             'rformat': fields.Str(missing="json",description="unimplemented: format of the response payload (json,json-ld)")
              }

queryargsnp = {'usernames': fields.Str(description="csv list of users the Workflows Executons are associated with"),
             'terms': fields.Str(description="csv list of metadata or parameter terms. These relate positionally to the expressions"),
             'expressions': fields.Str(description="csv list of metadata or parameters expressions. These relate positionally to the terms"),
             'wasAssociatedWith': fields.Str(description="csv lis of Components involved in the Workflow Executions"),
             'functionNames' : fields.Str(description="csv list of functions that are executed by at least one workflow's components"),
             'clusters' : fields.Str(description="csv list of clusters that describe and group one or more workflow's component"),
             'types': fields.Str(),
             'mode': fields.Str(description="execution mode of the workflow in case it support different kind of concrete mappings (eg. mpi, simple, multiprocess, etc.."),
             'formats': fields.Str(description="csv list of data formats (eg. mime-types)"),
             'rformat': fields.Str(missing="json",description="unimplemented: format of the response payload (json,json-ld)")
              }


queryargsnpdata = {'usernames': fields.Str(description="csv list of users the Workflows Executons are associated with"),
             'terms': fields.Str(description="csv list of metadata or parameter terms. These relate positionally to the expressions"),
             'expressions': fields.Str(description="csv list of metadata or parameters expressions. These relate positionally to the terms"),
             'functionNames' : fields.Str(description="csv list of functions the Data was generated with"),
             'types': fields.Str(description="csv list of data types"),
             'formats': fields.Str(description="csv list of data formats (eg. mime-types)"),
             'rformat': fields.Str(missing="json",description="unimplemented: format of the response payload (json,json-ld)")
              }

queryargs =dict(queryargsnp,**paging)

@app.route("/workflowexecutions/<runid>", methods=['GET', 'DELETE'])
@doc(tags=['discovery'], description='Extract documents from the bundle collection by the runid of a WFExecution. The method will return input data and infomation about the components and the libraries used for the specific run')
def get_workflow_info(runid):
        response=None
        if request.method == 'GET':
            response = Response(json.dumps(app.db.getRunInfo(runid)))

        elif request.method == 'DELETE' :
             if (len(runid)<=40):

                  response = Response(self.provenanceStore.deleteRun(runid))
             else:
                  response = {'success':False, 'error':'Invalid Run Id'}

                  if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":DELETE WorkflowRunInfo - "+self.path+" PID:"+str(os.getpid()));
        response.headers['Content-type'] = 'application/json'
        return response

# Thomas
#Extract documents from the bundle collection according to a query string which may include \id{username}, \id{type} of the workflow, its \id{functionNames} and domain metadata \id{terms} and \emph{value-ranges}.
@app.route("/workflowexecutions")
@use_kwargs(queryargs, locations=["querystring"])
@doc(tags=['discovery'], description='Extract documents from the bundle collection according to a query string which may include usernames, type of the workflow, the components the run wasAssociatedWith and their implementations. Data results\' metadata and parameters can also be queried by specifying the terms and either their min and max values-ranges (delimited with "...", min or max can be omitted), a comma-separate list of values, a single value (e.g a boolean) as well as their data formats. Mode of the search can also be indicated (mode ::= (OR j AND). It will apply to the search upon metadata and parameters values of each run')
def get_workflowexecutions(**kwargs):
    # Required parameters
    limit = kwargs['limit']
    start = kwargs['start']
    usernames = next(csv.reader(StringIO(kwargs['usernames']))) if 'usernames' in kwargs else None

    # include components parameters
    keylist = next(csv.reader(StringIO(kwargs['terms']))) if ('terms' in kwargs and kwargs['terms']!="") else None
    expressions = next(csv.reader(StringIO(kwargs['expressions']))) if ('expressions' in kwargs and kwargs['expressions']!="") else None
    wasAssociatedWith = next(csv.reader(StringIO(kwargs['wasAssociatedWith']))) if 'wasAssociatedWith' in kwargs else None
    implementations = next(csv.reader(StringIO(kwargs['functionNames']))) if ('functionNames' in kwargs and kwargs['functionNames']!="") else None
    clusters = next(csv.reader(StringIO(kwargs['clusters']))) if ('clusters' in kwargs and kwargs['clusters']!="") else None
    formats = next(csv.reader(StringIO(kwargs['formats']))) if ('formats' in kwargs and kwargs['formats']!="" and kwargs['formats']!="null") else None
    types = next(csv.reader(StringIO(kwargs['types']))) if ('types' in kwargs and kwargs['types']!="") in kwargs else None
    mode = kwargs['mode'] if 'mode' in kwargs else 'OR'

    if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET workflowexecutions -  PID:"+str(os.getpid()));
    response = Response()

    if keylist == None and implementations == None and formats==None and usernames!=None and clusters == None:

        result = app.db.getWorkflowExecution(int(start),int(limit),usernames=usernames)
        response = Response(json.dumps(result))
    else:
        response = Response(json.dumps(app.db.getWorkflowExecutionByLineage(int(start),int(limit),usernames=usernames, associatedWith=wasAssociatedWith, implementations=implementations, keylist=keylist,expressions=expressions, mode=mode, types=types,formats=formats,clusters=clusters)))

    response.headers['Content-type'] = 'application/json'
    return response


#Extract information about the invocation or instances related to specified \emph{WFExecution} (\id{run\_id}), such as \emph{lastEventTime}, runtime \emph{messages}, indication on the generation of data, its accessibility and its total count. Such a result-set can be used for runtime monitoring, showing progress, data availability and anomalies. Details about a single invocation or  an instance can also be accessed by specifying its $id$.
@app.route("/workflowexecutions/<runid>/instances")
@use_kwargs(paging,locations=["querystring"])
def get_instances_monitoring(runid,**kwargs):
    limit = kwargs['limit']
    start = kwargs['start']
    if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET workflowexecutions instances - "+runid+" PID:"+str(os.getpid()));
    response = Response()
    response = Response(json.dumps(app.db.getMonitoring(runid,'instance',int(start),int(limit))))
    response.headers['Content-type'] = 'application/json'
    return response

levelargsnp=dict({"level":fields.Str(required=True,description="level of depth in the data derivation graph, starting from the current Data")})
levelargs=dict({"level":fields.Str(enum=["component", "instance", "invocation", "cluster"],description="level of aggregation of the monitoring information (component, instance, invocation, cluster)")},**paging)

@app.route("/workflowexecutions/<runid>/showactivity")
@use_kwargs(levelargs,locations=["querystring"])
@doc(tags=['monitor'], description='Extract detailed information related to the activity related to a WFExecution (id). The result-set can be grouped by invocations, instances or components (parameter level) and shows progress, anomalies (such as exceptions or systems\' and users messages), occurrence of changes and the rapid availability of accessible data bearing intermediate results. This method can also be used for runtime monitoring')
def get_monitoring(runid,**kwargs):
    limit = kwargs['limit']
    start = kwargs['start']
    level = kwargs['level'] if 'level' in kwargs else None
    if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET workflowexecutions monitoring - "+runid+" PID:"+str(os.getpid()));
    response = Response()
    response = Response(json.dumps(app.db.getMonitoring(runid,level,int(start),int(limit))))
    response.headers['Content-type'] = 'application/json'
    return response

#Extract details about a single invocation or an instance by specifying their $id$.
@app.route("/invocations/<invocid>")
@doc(tags=['lineage'], description='Extract details about a single invocation by specifying its id')
def get_invocation_details(invocid):

        if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET invocation details - "+invocid+" PID:"+str(os.getpid()));
        response = Response()
        response = Response(json.dumps(app.db.getInvocation(invocid)))
        response.headers['Content-type'] = 'application/json'
        return response


#Extract details about a single invocation or an instance by specifying their $id$.
associatefor=dict({"wasAssociateFor":fields.Str(description='cvs list of runIds the instance was wasAssociateFor (when more instances are reused in multiple workflow executions)')},**paging)
@app.route("/instances/<instid>")
@use_kwargs(associatefor,locations=["querystring"])
@doc(tags=['lineage'], description='Extract details about a single instance or component by specifying its id. The returning document will indicate the changes that occurred, reporting the first invocation affected. It support the specification of a list of runIds the instance was wasAssociateFor, considering that the same instance could be used across multiple runs')
def get_instance_details(instid,**kwargs):
        limit = int(kwargs['limit']) if 'limit' in kwargs else None
        start = int(kwargs['start']) if 'start' in kwargs else None
        runIds = next(csv.reader(StringIO(kwargs['wasAssociateFor']))) if 'wasAssociateFor' in kwargs else None
        if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET instance details - "+instid+" PID:"+str(os.getpid()));
        response = Response()
        response = Response(json.dumps(app.db.getComponentInstance(instid,runIds=runIds,start=start,limit=limit)))
        response.headers['Content-type'] = 'application/json'
        return response

#instances=dict({"wasAssociateFor":fields.Str()},**paging)
@app.route("/components/<compid>")
@doc(tags=['lineage'], description='Extract details about a single component by specifying its id and the workflow run it wasAssociateFor, considering that the the same components could be used across multiple runs. The returning document will indicate the changes that occurred, reporting the first invocation affected')
@use_kwargs(associatefor,locations=["querystring"])
def getComponentDetails(compid, **kwargs):
        limit = int(kwargs['limit']) if 'limit' in kwargs else None
        start = int(kwargs['start']) if 'start' in kwargs else None
        runIds = next(csv.reader(StringIO(kwargs['wasAssociateFor']))) if 'wasAssociateFor' in kwargs else None
        if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET component details - "+compid+" PID:"+str(os.getpid()));
        response = Response()
        response = Response(json.dumps(app.db.getComponent(compid,runIds=runIds,start=start,limit=limit)))
        response.headers['Content-type'] = 'application/json'
        return response


@app.route("/data/<data_id>",methods=['GET'])
@doc(tags=['lineage'], description='Extract Data and their DataGranules by the Data id')
def get_data_item(data_id):

    response = Response(json.dumps(app.db.getData(0,1,id=str(data_id))))
    response.headers['Content-type'] = 'application/json'
    return response

#get_data_item.provide_automatic_options = False
#get_data_item.methods = ['GET']
#app.add_url_rule("/data/<data_id>", get_data_item)

# Thomas
#The data is selected by specifying its id or a $query\_string$. Query parameters allow to search by \emph{attribution}, \emph{generation} and by combining more metadata terms with their \emph{value-ranges}. Attribution will match all entities of the S-PROV model such as \emph{ComponentInstances}, \emph{Components}, \emph{prov:Person},  while generation will consider \emph{Invocation} and \emph{WorkflowExecution}.
dataargs=dict({'wasGeneratedBy':fields.Str(description='the id of the Invocation that generated the Data'),
               'wasAttributedTo':fields.Str(description='csv list of Component or Component Instances involved in the generation of the Data')},**queryargsnpdata)

dataargs =dict(dataargs,**paging)

@app.route("/data",methods=['GET'])
@doc(tags=['lineage'], description='The data is selected by specifying a query string. Query parameters allow to search by attribution to a component or to an implementation, generation by a workflow execution and by combining more metadata and parameters terms with their values (as single value, range list). Mode of the search can also be indicated (mode ::= (OR | AND). It will apply to the search upon metadata and parameters values-ranges')
@use_kwargs(dataargs,locations=["querystring"])
def get_data(**kwargs):
    limit = kwargs['limit']
    start = kwargs['start']
    if logging == "True" : app.logger.info(str(datetime.datetime.now().time())+":GET data collection - PID:"+str(os.getpid()));
    #run
    genby = kwargs['wasGeneratedBy'] if 'wasGeneratedBy' in kwargs else None
    #component
    attrTo = kwargs['wasAttributedTo'] if 'wasAttributedTo' in kwargs else None
    #implementation
    implementations = kwargs['functionNames'] if ('functionNames' in kwargs and kwargs['functionNames']!="") else None
    clusters = kwargs['clusters'] if ('clusters' in kwargs and kwargs['clusters']!="") else None
    keylist = next(csv.reader(StringIO(kwargs['terms']))) if 'terms' in kwargs else None
    expressions = next(csv.reader(StringIO(kwargs['expressions']))) if 'expressions' in kwargs else None
    format = kwargs['formats'] if 'formats' in kwargs else None
    mode = kwargs['mode'] if 'mode' in kwargs else 'OR'
    #id = kwargs['id'] if 'id' in kwargs else None

    response = Response(json.dumps(app.db.getData(int(start),int(limit),impl=implementations,genBy=genby,attrTo=attrTo,keylist=keylist,expressions=expressions,id=None,format=format,mode=mode,clusters=clusters)))

    response.headers['Content-type'] = 'application/json'

    return response

#Thomas
@app.route("/data/<data_id>/derivedData")
@use_kwargs(levelargsnp,locations=["querystring"])
@doc(tags=['lineage'], description='Starting from a specific data entity of the data dependency is possible to navigate through the derived data or backwards across the element\'s data dependencies. The number of traversal steps is provided as a parameter (level).')
def derived_data(data_id,**kwargs):
    level = kwargs['level']
    if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":GET derivedData - "+data_id+" PID:"+str(os.getpid()));
    response = Response(json.dumps(app.db.getDerivedDataTrace(data_id,int(level))))
    response.headers['Content-type'] = 'application/json'
    return response

#Thomas
@app.route("/data/<data_id>/wasDerivedFrom")
@use_kwargs(levelargsnp,locations=["querystring"])
@doc(tags=['lineage'], description='Starting from a specific data entity of the data dependency is possible to navigate through the derived data or backwards across the element\'s data dependencies. The number of traversal steps is provided as a parameter (level).')
def was_derived_from(data_id,**kwargs):
    level = kwargs['level']
    if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":GET wasDerivedFrom - "+data_id+" PID:"+str(os.getpid()));
    app.db.count=0




    #start_time = time.time()

    result=app.db.getTrace(data_id,int(level))
    #elapsed_time = time.time() - start_time
    #print(app.db.count)
    #print("ETIME "+str(elapsed_time))
    result=app.db.addLDContext(result[0])
    response = Response(json.dumps(result))
    response.headers['Content-type'] = 'application/json'
    return response






# Thomas
#@app.route("data/<data_id>/hasAncestorWith")

# Thomas
#Returns a list of metadata terms that can be suggested based on their appearance within a list of runs, users, or for the whole provenance archive
termsargs=dict({'runIds':fields.Str(description="csv list of run ids"),
                'usernames':fields.Str(description="csv list of usernames"),
                'aggregationLevel':fields.Str(enum=['all', 'runId','username'], description="set whether the terms need to be aggreagated by runId, username or across the whole collection (all)")})

@app.route("/terms")
@use_kwargs(termsargs,locations=["querystring"])
@doc(tags=['lineage'], description='Return a list of discoverable metadata terms based on their appearance for a list of runIds, usernames, or for the whole provenance archive. Terms are returned indicating their type (when consistently used), min and max values and their number occurrences within the scope of the search')
def get_data_granule_terms(**kwargs):
        aggregationLevel = kwargs['aggregationLevel'] if 'aggregationLevel' in kwargs else 'all'
        runIdList = next(csv.reader(StringIO(kwargs['runIds']))) if 'runIds' in kwargs else None
        usernameList = next(csv.reader(StringIO(kwargs['usernames']))) if 'usernames' in kwargs else None
        print('----->', aggregationLevel, runIdList, usernameList)

        respjson={}
        data = app.db.getDataGranuleTerms(aggregationLevel=aggregationLevel,runIdList=runIdList,usernameList=usernameList)
        respjson["metadata"]=data["_id"]
        respjson["terms"]=[]

        for x in data["value"]["contentMap"]:
            respjson["terms"].append({"term":x,"use":"metadata","valuesByType":data["value"]["contentMap"][x]["valuesByType"]})

        for x in data["value"]["parameterMap"]:
            respjson["terms"].append({"term":x,"use":"parameter","valuesByType":data["value"]["parameterMap"][x]["valuesByType"]})


        response = Response(json.dumps(respjson))

        response.headers['Content-type'] = 'application/json'



        return response

summaryargs=dict({'runId':fields.Str(description='the id of the run to be analysed'),
                  'groupby':fields.Str(description='express the grouping of the returned data'),
                  'mintime':fields.Str(description='minimum start time of the Invocation'),
                  'maxtime':fields.Str(description='maximum start time of the Invocation'),
                  'minidx':fields.Int(description='minimum iteration index of an Invocation'),
                  'maxidx':fields.Int(description='maximum iteration index of an Invocation')

                  },**levelargsnp)

@app.route("/summaries/workflowexecution")
@use_kwargs(summaryargs,locations=["querystring"])
@doc(tags=['summaries'], description='Produce a detailed overview of the distribution of the computation, reporting the size of data movements between the workflow components, their instances or invocations across worker nodes, depending on the specified granularity level. Additional information, such as process pid, worker, instance or component of the workflow (depending on the level of granularity) can be selectively extracted by assigning these properties to a groupBy parameter. This will support the generation of grouped views')
def summaries_handler_workflow(**kwargs):
        if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+": GET getSummaries workflow - level= "+kwargs['level']);
        #print(kwargs)
        # the db function can be split in two to serve worklfow executions and collaborative views with explicit parameters
        response = Response(json.dumps(app.db.getActivitiesSummaries(**kwargs)))
        response.headers['Content-type'] = 'application/json'
        return response

# Thomas check value-range level
# Extract information about the reuse and exchange of data between workflow executions, users and infrastructures, based terms and values' ranges. These Additional properties, such as workflow's type or (\id{prov:type})  can be also extracted

colargs=dict(dict({'groupby':fields.Str(description='express the grouping of the returned data')},**queryargsnp),**levelargsnp)

@app.route("/summaries/collaborative")
@use_kwargs(colargs,locations=["querystring"])
@doc(tags=['summaries'], description='Extract information about the reuse and exchange of data between workflow executions based on terms\' valuesranges and a group of users. The API method allows for inclusive or exclusive (mode ::= (OR j AND) queries on the terms\' values. As above, additional details, such as running infrastructure, type and name of the workflow can be selectively extracted by assigning these properties to a groupBy parameter. This will support the generation of grouped views')
def summaries_handler_collab(**kwargs):
        users = next(csv.reader(StringIO(kwargs['usernames']))) if 'usernames' in kwargs else None
        groupby = kwargs['groupby'] if 'groupby' in kwargs else None
        mode = kwargs['mode']if 'mode' in kwargs else None
        keylist = next(csv.reader(StringIO(kwargs['terms']))) if 'terms' in kwargs else None
        maxvalues = next(csv.reader(StringIO(kwargs['maxvalues']))) if 'maxvalues' in kwargs else None
        minvalues = next(csv.reader(StringIO(kwargs['minvalues']))) if 'minvalues' in kwargs else None

        

        if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+": GET getSummaries collab - level= "+kwargs['level']);

        response = Response(json.dumps(app.db.getCollaborativeSummariesWorkfows(mode=mode,groupby=groupby,users=users,keylist=keylist,maxvalues=maxvalues,minvalues=minvalues)))

        response.headers['Content-type'] = 'application/json'
        return response



exportprov=dict({'format':fields.Str(description="export format of the PROV document returned",enum=['rdf', 'json','xml','provn']),'rdfout':fields.Str(missing='trig',description="export rdf format of the PROV document returned",enum=['xml', 'n3', 'nt', 'trix','trig','turtle']),'creator':fields.Str(description="the name of the user requesting the export")})
exportdata=dict(exportprov,**levelargsnp)
# EXPORT to PROV methods

@app.route("/data/<data_id>/export")
@doc(tags=['export'], description='Export of provenance information PROV-XML or RDF format. The S-PROV information returned covers the whole workflow execution or is restricted to a single data element. In the latter case, the graph is returned by following the derivations within and across runs. A level parameter allows to indicate the depth of the resulting trace')
@use_kwargs(exportdata,locations=["querystring"])
def export_data_provenance(data_id,**kwargs):

    if 'creator' in kwargs:
      creator = kwargs['creator']
      del kwargs['creator']
    else:
      creator =  "anonymous"
    response = Response(str(app.db.exportDataProvenance(data_id,creator,**kwargs)).encode('ascii','ignore'))
    if 'format' in kwargs and kwargs['format']=='rdf':
        response.headers['Content-type'] = 'application/turtle'
    elif 'format' in kwargs and kwargs['format']=='json':
        response.headers['Content-type'] = 'application/json'
    elif 'format' in kwargs and kwargs['format']=='xml':
        response.headers['Content-type'] = 'application/xml'
    else:
        response.headers['Content-type'] = 'application/octet-streams'
    return response

queryargsanc=dict(dict({'ids':fields.Str(description="csv list of ids that needs to be filtered based on the query string parameters")},**queryargsbasic),**levelargsnp)
@app.route("/data/filterOnAncestor", methods=['POST'])
@use_kwargs(queryargsanc)
@doc(tags=['lineage'], description='Filter a list of data ids based on the existence of at least one ancestor in their data dependency graph, according to a list of metadata terms and their min and max values-ranges. Maximum depth level and mode of the search can also be indicated (mode ::= (OR | AND)')
def filter_on_ancestor(**kwargs):

        idlist = next(csv.reader(StringIO(kwargs['ids'])))
        keylist = next(csv.reader(StringIO(kwargs['terms']))) if ('terms' in kwargs and kwargs['terms']!="") else None
        valuelist = next(csv.reader(StringIO(kwargs['expressions']))) if ('expressions' in kwargs and kwargs['expressions']!="") else None

        level =int(kwargs['level']) if 'level' in kwargs else 100
        mode = kwargs['mode'] if 'mode' in kwargs else 'OR'

        if logging == "True" :  app.logger.info(str(datetime.datetime.now().time())+":POST filterOnAncestor mode= "+str(mode)+" PID:"+str(os.getpid()));
        res=json.dumps(app.db.filterOnAncestorsValuesRange(idlist,keylist,valuelist,level=level,mode=mode))
        response = Response(res)
        response.headers['Content-type'] = 'application/json'
        return response


@app.route("/workflowexecutions/<run_id>/export")
@use_kwargs(exportprov,locations=["querystring"])
@doc(tags=['export'], description='Export of provenance information PROV-XML or RDF format. The S-PROV information returned covers the whole workflow execution or is restricted to a single data element. In the latter case, the graph is returned by following the derivations within and across runs. A level parameter allows to indicate the depth of the resulting trace')
def export_run_provenance(run_id,**kwargs):


    if 'creator' in kwargs:
      creator = kwargs['creator']
      del kwargs['creator']
    else:
      creator =  "anonymous"

    response = Response(str(app.db.exportRunProvenance(run_id,creator,**kwargs)).encode('ascii','ignore'))

    if 'format' in kwargs and kwargs['format']=='rdf':
        response.headers['Content-type'] = 'application/turtle'
    elif 'format' in kwargs and kwargs['format']=='json':
        response.headers['Content-type'] = 'application/json'
    elif 'format' in kwargs and kwargs['format']=='xml':
        response.headers['Content-type'] = 'application/xml'
    else:
        response.headers['Content-type'] = 'application/octet-streams'
    return response



@app.errorhandler(422)
def handle_validation_error(err):
    exc = err.exc
    response = Response(json.dumps({'errors': exc.messages}))
    response.headers['Content-type'] = 'application/json'
    return response



if __name__ == "__main__":
    import sys
    #app.db = provenance.ProvenanceStore("mongodb://127.0.0.1/verce-prov")
    logging=False;

    # FS: Needs to be disabled for integration testing:
    # stream_handler = logging.StreamHandler()
    # stream_handler.setLevel(logging.DEBUG)
    # app.logger.addHandler(stream_handler)


    #app.add_url_rule('/stores', view_func=StoreResource.as_view('Store'))

    app.run()
    # app.logger.info("Server running....")


docs = FlaskApiSpec(app)


docs.register(insert_provenance)
docs.register(import_provenance)
docs.register(wfexec_description_edit)
docs.register(delete_workflow_run)

docs.register(get_workflow_info)
docs.register(get_workflowexecutions)
#docs.register(get_instances_monitoring)
docs.register(get_monitoring)
docs.register(get_invocation_details)
docs.register(get_instance_details)

docs.register(filter_on_ancestor)
docs.register(get_data_item)
docs.register(get_data)
docs.register(was_derived_from)
docs.register(derived_data)
docs.register(get_data_granule_terms)
docs.register(derived_data)

docs.register(summaries_handler_workflow)
docs.register(summaries_handler_collab)

docs.register(export_data_provenance)
docs.register(export_run_provenance)






