#!/usr/bin/python

# ***********************************************************************
# *                      All rights reserved                           **
# *                     Copyright (c) 2019 KNMI                        **
# *             Royal Netherlands Meteorological Institute             **
# ***********************************************************************
# * Function   : Convert CWLProv to S-Prov
# * Purpose    : viable CWLProv to S-Prov mapper
# * Usage      :
# *
# * Project    : DARE
# *
# * initial programmer :  Joske Brandsema
# * initial date       :  20190417
# TODO: check on existence of keys before access
# TODO: Specfem workflow input section is empty now.
# **********************************************************************


import json
import argparse
import uuid
import os

CWL_PRIMARY_PROV_FILE = "primary.cwlprov.json"

S_PROV_FILE_EXTENSION = ".sprov"

verbose = False

# CWL prov key words
CWLPROV_BASENAME = "cwlprov:basename"

PROV_ACTIVITY = "prov:activity"
PROV_COLLECTION = "prov:collection"
PROV_COLLECTION_VALUE = "prov:Collection"   # Mind the upper 'C'
PROV_DICTIONARY = "prov:Dictionary"
PROV_ENTITY = "prov:entity"
PROV_KEY_ENTITY_PAIR = "prov:KeyEntityPair"
PROV_PAIR_ENTITY = "prov:pairEntity"
PROV_PAIR_KEY = "prov:pairKey"
PROV_QUALIFIED_NAME = "prov:QUALIFIED_NAME"
PROV_ROLE = "prov:role"
PROV_TIME = "prov:time"
PROV_TYPE = "prov:type"

RO_FOLDER = "ro:Folder"

WF4EVER_FILE = "wf4ever:File"

WFPROV_PROCESS_RUN = "wfprov:ProcessRun"
WFPROV_WORKFLOW_RUN = "wfprov:WorkflowRun"

ACTIVITY = "activity"
ENTITY = "entity"
HAD_MEMBER = "hadMember"
TYPE = "type"
USED = "used"
WAS_GENERATED_BY = "wasGeneratedBy"

# sProv keywords
DERIVATION_IDS = "derivationIds"
DERIVED_FROM_DATASET_ID = "DerivedFromDatasetID"
END_TIME = "endTime"
ID = "id"
_ID = "_id"
INPUT = "input"
NAME = "name"
NOT_FOUND = "notFound"  # default sProv attribute is item is not found in CWL prov
RUN_ID = "runId"
START_TIME = "startTime"
STREAMS = "streams"
USERNAME = "username"
WAS_ENDED_BY = "wasEndedBy"
WAS_STARTED_BY = "wasStartedBy"


def is_item_type(item, item_type):
    """
    Return True if entity or activity is type. In other words if type is mentioned in the provtype
    :param item: a CWLProv entity or activity
    :param item_type:
    :return:
    """
    item_is_type = False
    if PROV_TYPE in item:
        if type(item[PROV_TYPE]) == list:
            for prov_type in item[PROV_TYPE]:
                if prov_type["$"] == item_type:
                    item_is_type = True
        elif type(item[PROV_TYPE]) == dict:
            if item[PROV_TYPE]["$"] == item_type:
                item_is_type = True
    return item_is_type


def write_json_file(file_path, content):
    """
    :param file_path:
    :param content:
    :return:
    """
    with open(file_path, 'w') as outfile:
        json.dump(content, outfile, indent=4)


def get_workflow_activity_id(cwl_prov_data):
    """
    Assume there is only one WorkflowRun in a cwlProvData document, otherwise return last id
    :param cwl_prov_data:
    :return: workflow_run_activity_id
    """
    global workflow_run_activity_id
    for key, value in cwl_prov_data[ACTIVITY].items():
        if (type(value) == dict and
            value[PROV_TYPE]["$"] == WFPROV_WORKFLOW_RUN):
                workflow_run_activity_id = key
                break
    if verbose:
        print("  Found WorkflowRun: %s" % workflow_run_activity_id)
    return workflow_run_activity_id


def get_process_run_activity_ids(cwl_prov_data):
    """
    Returns a list of of all the ProcessRunIds in a cwlProvData document
    :param cwl_prov_data:
    :return: process_activities
    """
    process_activities = []
    for key, value in cwl_prov_data[ACTIVITY].items():
        if (type(value) == dict and
                value[PROV_TYPE]["$"] == WFPROV_PROCESS_RUN):
            process_activities.append(key)
    return process_activities


def get_username(cwl_prov_data):
    """
    Returns username from agent in cwlProvDocument
    :param cwl_prov_data:
    :return: username
    """
    username = NOT_FOUND
    for key, value in cwl_prov_data["agent"].items():
        if is_item_type(value, "prov:Person"):
            username = value["prov:label"]
            break
    return username


def get_used_item_from_dictionary_entity(dictionary_id, cwl_prov_data):
    """
    Return attributes of a used item as a dictionary. A dictionary entity contains the value of an attribute,
    the type of an attribute (you find in the pairEntity entities) and the id of the pairEntity
    :param dictionary_id: the entity id of collection containing the entities with the attributes of an used element
    :param cwl_prov_data:
    :return:
    """
    # loop over the artifact collection, which contains entities with URL, filename and ID
    used_item = {}
    for had_member_key, had_member_value in cwl_prov_data[HAD_MEMBER].items():
        if had_member_value[PROV_COLLECTION] == dictionary_id:
            attribute_id = had_member_value[PROV_ENTITY]
            if "prov:value" in cwl_prov_data[ENTITY][attribute_id].keys():
                attribute_value = cwl_prov_data[ENTITY][attribute_id]["prov:value"]
            else:
                continue
            # Find the type of this attribute in corresponding KeyEntityPair
            for entity_key, entity_val in cwl_prov_data[ENTITY].items():
                if (PROV_PAIR_ENTITY in entity_val and
                        entity_val[PROV_PAIR_ENTITY]["$"] == attribute_id):
                    attribute_type = entity_val[PROV_PAIR_KEY]
                    used_item[attribute_type] = attribute_value
                    break
    return used_item


def get_file_dictionary_from_entity(entity_id, cwl_prov_data, start_time=None, end_time=None):
    """
    Creates a dictionary from entity with entity_id containing the following file attributes:
        id of entity, basename, name, starttime and endtime
    :param entity_id:
    :param cwl_prov_data:
    :param start_time:
    :param end_time:
    :return:
    """

    # TODO: For now we are using NOT_FOUND to be able to run. Asuming when the entity is a collection the name is referenced rather then available via cwlprov:basename
    if CWLPROV_BASENAME in cwl_prov_data[ENTITY][entity_id]:
        basename = cwl_prov_data[ENTITY][entity_id][CWLPROV_BASENAME]
    else:
        basename = NOT_FOUND

    # sProv doesn't seem to know the key CWLPROV_BASENAME. So adding also key NAME with value basename

    file_dict = {PROV_TYPE: WF4EVER_FILE,
                 ID: entity_id,
                 CWLPROV_BASENAME: basename,
                 NAME: basename}
    if start_time is not None:
        file_dict[START_TIME] = start_time
    if end_time is not None:
        file_dict[END_TIME] = end_time

    return file_dict


def get_activity_id_was_generated_by(entity_id, activity_type, cwl_prov_data):
    """
    Get the activity ID with type activity_type that generated the entity with id entity_id
    The activity can be directly related by WAS_GENERATED_BY or indirect via HAD_MEMBER to a collection
    :param entity_id: a CWLProv entity_id
    :param activity_type: is an wfprov:WorkflowRun or wfprov:ProcessRun
    :param cwl_prov_data:
    :return: activity ID
    """
    activity_id = None
    look_for_entity = entity_id

    for key, value in cwl_prov_data[HAD_MEMBER].items():
        if value[PROV_ENTITY] == entity_id:
            look_for_entity = value[PROV_COLLECTION]

    for key, value in cwl_prov_data[WAS_GENERATED_BY].items():
        if (value[PROV_ENTITY] == look_for_entity and
                is_item_type(cwl_prov_data[ACTIVITY][value[PROV_ACTIVITY]], activity_type)):
            activity_id = value[PROV_ACTIVITY]
            break

    return activity_id


def get_used_items(activity_id, cwl_prov_data):
    """
    Returns list of the USED items of activity_id. Each element of the list is a dictionary with attributes of an item.
    USED can point to: a collection of collections with attribute entities
                         a collection with attribute entities.
                         a folder (for Specfem)
                         a file (for Specfem)

    :param activity_id:
    :param cwl_prov_data:
    :return: used
    """
    used = []
    # Find the USED collection of WorkflowRun
    # Assume only 1 USED association, so no list
    if USED not in cwl_prov_data.keys():
        return ""
    for key, value in cwl_prov_data[USED].items():
        if value[PROV_ACTIVITY] == activity_id:

            item = None
            used_entity_key = value[PROV_ENTITY]
            # The leaf key contains the actual "file"
            used_entity_key_leaf = used_entity_key

            # If it is a Folder, Dictionary or File we can get the attributes of the entity
            # If it is a Collection of Collections, we have to find all dictionaries.
            #
            # RO_FOLDER and WF4EVER_FILE are as found in the Specfem Workflow activity
            # An entity can be several types, therefore folder is tested before dictionary
            if is_item_type(cwl_prov_data[ENTITY][used_entity_key], RO_FOLDER):
                item = {ID: used_entity_key, PROV_TYPE: RO_FOLDER}
            elif is_item_type(cwl_prov_data[ENTITY][used_entity_key], PROV_DICTIONARY):
                item = get_used_item_from_dictionary_entity(
                    used_entity_key, cwl_prov_data)
            elif is_item_type(cwl_prov_data[ENTITY][used_entity_key], WF4EVER_FILE):
                item = get_file_dictionary_from_entity(
                    used_entity_key, cwl_prov_data)
            else:
                item = []
                for had_member_key, had_member_value in cwl_prov_data[HAD_MEMBER].items():
                    if had_member_value[PROV_COLLECTION] == used_entity_key:
                        used_entity_key_leaf = had_member_value[PROV_ENTITY]
                        if is_item_type(cwl_prov_data[ENTITY][used_entity_key_leaf], PROV_DICTIONARY):
                            item.append(get_used_item_from_dictionary_entity(
                                used_entity_key_leaf, cwl_prov_data))
                        else:
                            # The type is unknown, therefore the id is just added
                            # e.g. found type wf4ever:File in ProcessRun in primary document.
                            item.append({ID: used_entity_key_leaf})
            activity_id_was_generated_by = get_activity_id_was_generated_by(
                used_entity_key_leaf, WFPROV_PROCESS_RUN, cwl_prov_data)

            if activity_id_was_generated_by is not None:
                item["TriggeredByProcessIterationID"] = activity_id_was_generated_by

            if item is not None:
                if type(item) == dict:
                    used.append(item)
                else:
                    used = item
            else:
                print("    STRANGE! Used item is unknown")
                print("      activity:    %s" % value[PROV_ACTIVITY])
                print("      entity:      %s" % used_entity_key)
                print("      leaf entity: %s" % used_entity_key_leaf)
    return used


def get_entity_list_from_collections(entity_id, cwl_prov_data, collection_path=""):
    """
    Get a list by recursively follow embedded collections starting with entity_id.
    If entity_id is not a collection, return a list with only the entity_id itself.
    Using a dictionary makes adding items to the entity list easy. Downside is that
    the collection path can't be a list, lists are unhashable so can not be keys. 
    :param entity_id: collection entity id
    :param cwl_prov_data:
    :param start_collection_path: the paths of collections entity_id is part of
    :return: dictionary; keys are the collection paths, values are lists of 'file' entities belonging to the collection path 
    """
    return_dict = {}
    if is_item_type(cwl_prov_data[ENTITY][entity_id], PROV_COLLECTION_VALUE):
        # If type changed, this might need deep copy
        collection_path += "/" + entity_id
        if verbose:
            print("       get_entity_list_from_collections: found collection at depth %d %s"
                  % (len(collection_path.split("/"))-1, collection_path))

        for had_member_key, had_member_value in cwl_prov_data[HAD_MEMBER].items():
            if had_member_value[PROV_COLLECTION] == entity_id:
                answer = get_entity_list_from_collections(
                    had_member_value[PROV_ENTITY], cwl_prov_data, collection_path)
                for key, value in answer.items():
                    if key in return_dict:
                        return_dict[key] += value
                    else:
                        return_dict[key] = value

    else:
        if verbose:
            print("       get_entity_list_from_collections: Found entity %s in path %s" % (
                entity_id, collection_path))
        return_dict[collection_path] = [entity_id]

    return return_dict


def get_entity_name_via_pairkey(entity_id, cwl_prov_data):
    """
    For entities describing output files, the name is in the entity itself as cwlprov:basename. For collection entities we 
    have to find the matching pairKey entity.

    Get the name for the entity by looking for the prov:QUALIFIED_NAME (prov:KeyEntityPair) where the PROV_PAIR_ENTITY matches
    the entity_id. Returns empty string when nothing found.
    :param entity_id: collection entity id
    :param cwl_prov_data
    :return: name if found, otherwise empty string
    """
    return_name = ""
    for key, entity in cwl_prov_data[ENTITY].items():
        if (is_item_type(entity, PROV_KEY_ENTITY_PAIR)
            and PROV_QUALIFIED_NAME == entity[PROV_TYPE][TYPE]
                and entity_id == entity[PROV_PAIR_ENTITY]['$']):
            return_name = entity[PROV_PAIR_KEY]
    return return_name


def get_collection_location(collection_ids, cwl_prov_data):
    """
    Get location for a collection by creating a 'path' by adding the names of the collections in collection_ids
    :param collection_ids: List of collections in path
    :return: location
    """
    collection_location = ""
    for collection_id in collection_ids:
        name = get_entity_name_via_pairkey(collection_id, cwl_prov_data)
        if (name):
            collection_location += name + "/"
    if verbose:
        print("get_collection_location: location for %s is %s" %
              (collection_ids, collection_location))
    return collection_location


def add_generated_by_items(streams_list, entity_id, start_time, end_time, workflow_id, cwl_output_location, cwl_prov_data):
    """
    Get a list of dictionaries of all items containing their (file) attributes
    if the entity is a collection of file entities, it includes all the (file) members of this collection
    if the entity is a collection of collections of file entities, it follows the burried collection and returns all the (file) members 
    If a found generated item is also mentioned as generated by a workflow, we know this item is output of the entire workflow and available after the workflow
    If it is not mentioned as part of worklfow output, it is an intermediate file.
    :param streams_list: list to be expanded 
    :param entity_id: collection entity id
    :param start_time: Start time cannot be found in the entity, so it is needed from the level above (wasGeneratedBy)
    :param end_time: End time cannot be found in the entity, so it is needed from the level above (wasGeneratedBy)
    :param workflow_id:
    :param cwl_prov_data:
    :return: -
    """
    is_workflow_output = False

    generated_items = get_entity_list_from_collections(
        entity_id, cwl_prov_data)

    for collection_ids_path, item_id_list in generated_items.items():
        if verbose:
            print("add_generated_by_items: processing collection_path %s" %
                  (collection_ids_path))
        if verbose:
            print("                               containing itmes: %s" %
                  (item_id_list))

        if (not 0 == len(item_id_list)):
            item_list = []
            collection_ids = collection_ids_path.split(
                '/')[1:]                 # Skipping the first "/"

            location = get_collection_location(collection_ids, cwl_prov_data)

            # If the activity of the collection itself, or one of its members, is a workflowrun, add the location of the workflow.
            # The location of the workflow is not in the cwl provenance, but given as an argument to cwlprov_2_sprov
            if (collection_ids and
                    workflow_id == get_activity_id_was_generated_by(collection_ids[-1], WFPROV_WORKFLOW_RUN, cwl_prov_data)):
                # Is the collection self related to the workflowrun?
                    is_workflow_output = True
                    if verbose:
                        print("add_generated_by_items: activity related to collection %s is the main workflow %s" % (
                            collection_ids[-1], workflow_id))
            for item_id in item_id_list:
                # Is the member related to the workflowrun?
                if (workflow_id == get_activity_id_was_generated_by(item_id, WFPROV_WORKFLOW_RUN, cwl_prov_data)):
                    is_workflow_output = True
                    if verbose:
                        print("add_generated_by_items: activity related to item %s is the main workflow %s" % (
                            item_id, workflow_id))

            if not collection_ids:
                current_collection_id = item_id_list[-1]
            else:
                current_collection_id = collection_ids[-1]

            if is_workflow_output:
                if verbose:
                    print("      %s was generated by workflow %s, so adding %s to location of stream" % (
                        current_collection_id, workflow_id, cwl_output_location))
                location = cwl_output_location + "/" + location

            for item_id in item_id_list:
                item_list.append(get_file_dictionary_from_entity(
                    item_id, cwl_prov_data, start_time=start_time, end_time=end_time))

            streams_list.append(
                {ID: current_collection_id, "location": location, "content": item_list})

    if verbose:
        print("add_generated_by_times: Created streams_list:\n%s" % streams_list)


def generate_lineage_sprov(main_workflow_run_activity_id, process_run_activity_id, username, cwl_output_location, cwl_prov_data, runId=None):
    """
    Generates a Lineage S-Prov document of the "ProcessRun" activity with given id in CwlProvData
    :param main_workflow_run_activity_id: ID of the WorkflowRun in corresponding workflow document
    :param process_run_activity_id: ID of the ProcessRun in cwlProvData to generate the lineage document
    :param username:
    :param cwl_output_location: string. Location where to find the output of the CWL workflow. e.g. path or URL
    :param cwl_prov_data:
    :return: S-Prov Lineage dictionary
    """

    # Set initial value to variables
    lineage_sprov_data = {_ID: str(uuid.uuid4()), STREAMS: []}
    was_generated_by_step_label = NOT_FOUND
    was_generated_by_iteration = NOT_FOUND
    was_generated_by_command = NOT_FOUND
    was_generated_by_role = NOT_FOUND
    activity_start_time = NOT_FOUND
    activity_end_time = NOT_FOUND

    # Find startTime and endTime of activity
    for started_by_key, started_by_value in cwl_prov_data[WAS_STARTED_BY].items():
        if started_by_value[PROV_ACTIVITY] == process_run_activity_id:
            activity_start_time = started_by_value[PROV_TIME]
            break
    for started_by_key, started_by_value in cwl_prov_data[WAS_ENDED_BY].items():
        if started_by_value[PROV_ACTIVITY] == process_run_activity_id:
            activity_end_time = started_by_value[PROV_TIME]
            break

    # Find the output streams (wasGeneratedBy)
    for generated_key, generated_value in cwl_prov_data[WAS_GENERATED_BY].items():
        if generated_value[PROV_ACTIVITY] == process_run_activity_id:
            was_generated_by_entity = generated_value[PROV_ENTITY]
            was_generated_by_time = generated_value[PROV_TIME]

            add_generated_by_items(lineage_sprov_data[STREAMS], was_generated_by_entity, activity_start_time,
                                   was_generated_by_time, main_workflow_run_activity_id, cwl_output_location, cwl_prov_data)

            # For S-Prov name we use the <command> in cwl prov:role <main>/<step-label>_<step-iteration-nr>/<command>
            # Note: The S-Prov lineage NAME will be the name of the last stream found.
            # For now no issue because there's only one stream per ProcessRun activity
            was_generated_by_command = generated_value[PROV_ROLE]["$"].split(
                "/")[-1]

            # For S-Prov instanceID we use the <step-label> in cwl prov:role <main>/<step-label>_<step-iteration-nr>/<command>
            # Notes:
            # - The first iteration doesn't have a <step-iteration-nr>; we assume iteration 1.
            # - If there are more wasGeneratedBy streams for a activity, the instanceId & iterationIndex will be the last one found.
            was_generated_by_step_iteration = generated_value[PROV_ROLE]["$"].split(
                "/")[-2].split("_")
            if (type(was_generated_by_step_iteration) == list) and (len(was_generated_by_step_iteration) == 2):
                if was_generated_by_step_iteration[1].isnumeric():
                    was_generated_by_step_label = was_generated_by_step_iteration[0]
                    was_generated_by_iteration = was_generated_by_step_iteration[1]
                else:     # last part of role is composed name with "_". i.e. create_environment
                    was_generated_by_step_label = generated_value[PROV_ROLE]["$"].split(
                        "/")[-2]
                    was_generated_by_iteration = 1
            else:
                was_generated_by_step_label = was_generated_by_step_iteration[0]
                was_generated_by_iteration = 1

            # Find the identical entity id in WAS_GENERATED_BY to get its role to use for the S-Prov actedOnBehalfOf
            for generated_workflow_key, generated_workflow_value in cwl_prov_data[WAS_GENERATED_BY].items():
                if (generated_workflow_value[PROV_ACTIVITY] != process_run_activity_id and
                        generated_workflow_value[PROV_ENTITY] == was_generated_by_entity):
                    was_generated_by_role = generated_workflow_value[PROV_ROLE]["$"].split(
                        "/")[-1]
                    break

    if was_generated_by_role == NOT_FOUND:
        was_generated_by_role = was_generated_by_step_label
        instance_id = was_generated_by_step_label + "-" + str(uuid.uuid4())
    else:
        instance_id = was_generated_by_step_label

    # Find startTime and endTime of activity
    for started_by_key, started_by_value in cwl_prov_data[WAS_STARTED_BY].items():
        if started_by_value[PROV_ACTIVITY] == process_run_activity_id:
            activity_start_time = started_by_value[PROV_TIME]
            break
    for started_by_key, started_by_value in cwl_prov_data[WAS_ENDED_BY].items():
        if started_by_value[PROV_ACTIVITY] == process_run_activity_id:
            activity_end_time = started_by_value[PROV_TIME]
            break

    if not runId: runId = main_workflow_run_activity_id
    # Set the global S-Prov attributes
    lineage_sprov_data[TYPE] = "lineage"
    lineage_sprov_data["actedOnBehalfOf"] = was_generated_by_role
    lineage_sprov_data["instanceId"] = instance_id
    lineage_sprov_data["iterationId"] = was_generated_by_command + \
        "-" + process_run_activity_id.replace(":", "-")
    lineage_sprov_data["prov_cluster"] = was_generated_by_role
    lineage_sprov_data[USERNAME] = username
    lineage_sprov_data["iterationIndex"] = was_generated_by_iteration
    lineage_sprov_data[RUN_ID] = runId #main_workflow_run_activity_id
    lineage_sprov_data[NAME] = was_generated_by_command
    lineage_sprov_data[START_TIME] = activity_start_time
    lineage_sprov_data[END_TIME] = activity_end_time

    # Find the input streams (used)
    lineage_sprov_data[DERIVATION_IDS] = get_used_items(
        process_run_activity_id, cwl_prov_data)
    # To be able to link the derivationIds to output of previous activities the ID should have DERIVED_FROM_DATASET_ID as key
    for n in range(len(lineage_sprov_data[DERIVATION_IDS])):
        if ID in lineage_sprov_data[DERIVATION_IDS][n]:
            lineage_sprov_data[DERIVATION_IDS][n][DERIVED_FROM_DATASET_ID] = lineage_sprov_data[DERIVATION_IDS][n].pop(
                ID)
        if "@id" in lineage_sprov_data[DERIVATION_IDS][n]:
            lineage_sprov_data[DERIVATION_IDS][n][DERIVED_FROM_DATASET_ID] = lineage_sprov_data[DERIVATION_IDS][n].pop(
                "@id")

    if verbose:
        print("Generated lineage document:")
        print("====================================================================================")
        print(json.dumps(lineage_sprov_data, indent=4, sort_keys=True))
        print("====================================================================================")
    return lineage_sprov_data


def generate_workflow_sprov(main_workflow_run_activity_id, username, cwl_prov_data, runId=None):
    """
    Generates the Sprov Workflow document from the cwl prov data.
    :param main_workflow_run_activity_id: ID of the activity in the cwl_prov_data
    :param username:
    :param cwl_prov_data:
    :return:  S-Prov workflow document
    """
    if verbose:
        print("\ngenerate_workflow_sprov for %s" %
              main_workflow_run_activity_id)

    workflow_run_start_time = cwl_prov_data[ACTIVITY][main_workflow_run_activity_id]["prov:startTime"]

    if runId: actual_run_id = runId
    else: actual_run_id = main_workflow_run_activity_id
    workflow_sprov_data = {
        TYPE: "workflow_run",
        _ID: actual_run_id,
        RUN_ID: actual_run_id,
        USERNAME: username,
        START_TIME: workflow_run_start_time,
        INPUT: get_used_items(main_workflow_run_activity_id, cwl_prov_data)
    }
    if verbose:
        print("Generated workflow document:")
        print("====================================================================================")
        print(json.dumps(workflow_sprov_data, indent=4, sort_keys=True))
        print("====================================================================================")
    return workflow_sprov_data


def get_has_provenance_filenames(cwl_prov_data):
    """
    Returns a list of filenames from the processRun activity with HasProvenance attributes
    :param cwl_prov_data:
    :return: filenames
    """
    filenames = []
    for key, value in cwl_prov_data[ACTIVITY].items():
        if type(value) == list:
            for activity_dict in cwl_prov_data[ACTIVITY][key]:
                for dict_key, dict_value in activity_dict.items():
                    if dict_key == "prov:has_provenance":
                        for file in dict_value:
                            file_basename, file_extension = os.path.splitext(
                                file["$"])
                            if file_extension == ".json":
                                filenames.append(file["$"].split(":")[-1])
    return filenames


def cwlprov_2_sprov(input_path, output_path, cwl_output_location, runId=None):
    """
    Find the CWLProv json documents under input_path and generate the corresponding S-Prov documents in output_path
    :param input_path:
    :param output_path:
    :param cwl_output_location: string. Location where to find the output of the CWL workflow. e.g. path or URL
    :param runId override the run id stored in the provenance document.
    :return:
    """
    # TODO test on existence of input_path and output_path
    # Find the primary primary-file: CWL_PRIMARY_PROV_FILE

    lineage_activities = []  # Keep a list of processed processRun activities
    primary_file_path = ""
    cwl_prov_path = ""
    for root, dirs, files in os.walk(input_path):
        if CWL_PRIMARY_PROV_FILE in files:
            cwl_prov_path = root
            primary_file_path = root + "/" + CWL_PRIMARY_PROV_FILE
            break

    if not primary_file_path:
        print("Error: Primary cwl provenance file %s not found in %s" %
              (CWL_PRIMARY_PROV_FILE, input_path))
        return None

    if verbose:
        print("Going to process %s" % CWL_PRIMARY_PROV_FILE)
    with open(primary_file_path, "r") as read_file:
        primary = json.load(read_file)

    username = get_username(primary)
    main_workflow_run_activity_id = get_workflow_activity_id(primary)

    # Find workflowRun activity in primary file and generate S-Prov workflow document
    workflow_sprov = generate_workflow_sprov(
        main_workflow_run_activity_id, username, primary, runId)
    out_filename = output_path + "/" + "workflow_" + \
        main_workflow_run_activity_id + S_PROV_FILE_EXTENSION

    write_json_file(out_filename, workflow_sprov)

    # Find all processRun activities in primary cwl prov file and create lineage document for each
    for process_activity in get_process_run_activity_ids(primary):
        if verbose:
            print("  Going to create S-Prov lineage for %s" % process_activity)
        process_sprov = generate_lineage_sprov(
            main_workflow_run_activity_id, process_activity, username, cwl_output_location, primary, runId)
        out_filename = output_path + "/" + "lineage_" + \
            process_activity.split(
                ":")[-1] + "_" + CWL_PRIMARY_PROV_FILE + S_PROV_FILE_EXTENSION
        write_json_file(out_filename, process_sprov)
        lineage_activities.append(process_activity)

    # Find all hasProvenance json files and generate lineage document of each unique activity
    for prov_filename in get_has_provenance_filenames(primary):
        prov_file_path = cwl_prov_path + "/" + prov_filename
        if verbose:
            print("Going to process %s" % prov_file_path)
        with open(prov_file_path, "r") as read_file:
            cwl_prov = json.load(read_file)
        # Ignore the activities that are already processed and are therefore a duplicate
        ignored_activities = []
        for process_activity in get_process_run_activity_ids(cwl_prov):
            if process_activity in lineage_activities:
                ignored_activities.append(process_activity)
            else:
                if verbose:
                    print("  Going to create S-Prov lineage for %s" %
                          process_activity)
                process_sprov = generate_lineage_sprov(
                    main_workflow_run_activity_id, process_activity, username, cwl_output_location, cwl_prov, runId)
                out_filename = output_path + "/" + "lineage_" + \
                    process_activity.split(
                        ":")[-1] + "_" + prov_filename + S_PROV_FILE_EXTENSION
                write_json_file(out_filename, process_sprov)
                lineage_activities.append(process_activity)
        if verbose:
            print("  Ignored %d duplicate activities" %
                  (len(ignored_activities)))

    return main_workflow_run_activity_id


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Convert CWLProv to S-Prov")
    parser.add_argument(
        INPUT, help="Datadir. Should contain file " + CWL_PRIMARY_PROV_FILE)
    parser.add_argument("output", help="Directory to store provn files")
    parser.add_argument("location", help="Location of CWL workflow output")
    parser.add_argument("-v", "--verbose", dest="verbose", action="store_true",
                        required=False, default=False,
                        help="Show verbose logging on stdout.")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()
    verbose = args.verbose
    cwlprov_2_sprov(args.input, args.output, args.location)
