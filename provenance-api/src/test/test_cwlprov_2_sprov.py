#!/usr/bin/python

# ***********************************************************************
# *                      All rights reserved                           **
# *                     Copyright (c) 2019 KNMI                        **
# *             Royal Netherlands Meteorological Institute             **
# ***********************************************************************
# * Function   : Test of Convert CWLProv to S-Prov
# * Purpose    : viable CWLProv to S-Prov mapper
# * Usage      :
# *
# * Project    : DARE
# *
# * initial programmer :  Joske Brandsema
# * initial date       :  20190506

import json
import unittest
import sys
import os
import shutil


# specfem  based on specfem on cwl provenance run (https://gitlab.com/project-dare/WP6_EPOS/tree/master/specfem3d/specfem3d_test_input_cwl)
#          inside container rosafilgueira/specfem3d_mpi:version1
#          Run on January 14, 2020
TEST_CASES = ["download", "specfem"]            

TEST_LOCATION = os.path.dirname(os.path.abspath(__file__))
OUTPUT_PATH = TEST_LOCATION + "/output"

IGNORED = "__ignored__"

VERBOSE = True

sys.path.append(TEST_LOCATION + "/../prov-services")
import cwlprov_2_sprov


class TestCwlProv2Sprov(unittest.TestCase):

    def load_json_file(self, path):
        with open(path, "r") as read_file:
            try:
                content = json.load(read_file)
            except:
                print("Failed to get json-data for", path)
                content = {}
        return content

    def find_file(self, path, filename):
        filepath = None
        for root, dirs, files in os.walk(path):
            if filename in files:
                filepath = root + "/" + filename
        return filepath

    def ignore_ignored(self, expected_content, output_content):
        for expected_json_leaf_path in self.dict_generator(expected_content):
            if expected_json_leaf_path[-1] == IGNORED:
                walking_output = output_content
                for path_item in expected_json_leaf_path[:-2]:
                    if isinstance(path_item, list):
                        walking_output = walking_output[path_item[0]]
                    else:
                        walking_output = walking_output[path_item]
                if isinstance(walking_output, list):
                    walking_output[expected_json_leaf_path[-2][0]] = IGNORED
                elif expected_json_leaf_path[-2] in walking_output:
                    walking_output[expected_json_leaf_path[-2]] = IGNORED
        return output_content

    def setUp(self):
        # create output directory
        print("setup")
        if not os.path.exists(OUTPUT_PATH):
            os.mkdir(OUTPUT_PATH)
        for test_case in TEST_CASES:
            test_case_path = OUTPUT_PATH + "/" + test_case
            if not os.path.exists(test_case_path):
                os.mkdir(test_case_path)

    def tearDown(self):
        # clean mess of output directory
        print("teardown")
        shutil.rmtree(OUTPUT_PATH)

    def dict_generator(self, indict, pre=None):
        pre = pre[:] if pre else []
        if isinstance(indict, dict):
            for key, value in indict.items():
                if isinstance(value, dict):
                    for d in self.dict_generator(value, pre + [key]):
                        yield d
                elif isinstance(value, list) or isinstance(value, tuple):
                    for n, v in enumerate(value):
                        for d in self.dict_generator(v, pre + [key] + [[n]]):
                            yield d
                else:
                    yield pre + [key, value]
        else:
            yield pre + [indict]

    def test_cwlprov_2_sprov(self):
        for test_case in TEST_CASES:

            ## To make the test more robust, test if the paths exist. This
            ## will prevent the test from succeeding if it fails to enter the
            ## loop below.
            input_path = os.path.join(TEST_LOCATION, "test_input", test_case)
            self.assertTrue(os.path.exists(input_path), msg="%s does not exist." % input_path)
            output_path = os.path.join(TEST_LOCATION, "output", test_case)
            self.assertTrue(os.path.exists(output_path), msg="%s does not exist." % output_path)
            expected_path = os.path.join(TEST_LOCATION, "expected", test_case)
            self.assertTrue(os.path.exists(expected_path), msg="%s does not exist." % expected_path)

            if VERBOSE: print("Running cwlprov_2_sprov for %s" % input_path)
            runid = cwlprov_2_sprov.cwlprov_2_sprov(input_path, output_path, "fancy_location")
            primjson = self.load_json_file(os.path.join(input_path, "metadata", "provenance", "primary.cwlprov.json"))
            self.assertIn(runid, primjson['activity'])

            for root, dirs, files in os.walk(expected_path):
                for expected_filename in files:
                    if VERBOSE: print("    Checking %s" % expected_filename)
                    expected_file = os.path.join(root, expected_filename)
                    self.assertTrue(os.path.exists(expected_file), msg="%s does not exist." % expected_file)

                    expected_json = self.load_json_file(expected_file)
                    self.assertEqual(expected_json['runId'], runid)

                    output_file_path = self.find_file(output_path, expected_filename)
                    self.assertIsNotNone(output_file_path, msg="Expected output file not found " + expected_filename)
                    output_json = self.load_json_file(output_file_path)

                    self.maxDiff = None

                    self.assertEqual(expected_json, self.ignore_ignored(expected_json, output_json))

    def test_cwlprov_2_sprov_custom_runid(self):
        for test_case in TEST_CASES:

            ## To make the test more robust, test if the paths exist. This
            ## will prevent the test from succeeding if it fails to enter the
            ## loop below.
            input_path = os.path.join(TEST_LOCATION, "test_input", test_case)
            self.assertTrue(os.path.exists(input_path), msg="%s does not exist." % input_path)
            output_path = os.path.join(TEST_LOCATION, "output", test_case)
            self.assertTrue(os.path.exists(output_path), msg="%s does not exist." % output_path)
            expected_path = os.path.join(TEST_LOCATION, "expected", test_case+"_custom_runid")
            self.assertTrue(os.path.exists(expected_path), msg="%s does not exist." % expected_path)

            if VERBOSE: print("Running cwlprov_2_sprov for %s" % input_path)
            runid = cwlprov_2_sprov.cwlprov_2_sprov(input_path, output_path, "fancy_location", "id:"+"runid-override")

            primjson = self.load_json_file(os.path.join(input_path, "metadata", "provenance", "primary.cwlprov.json"))
            self.assertIn(runid, primjson['activity'])

            for root, dirs, files in os.walk(expected_path):
                for expected_filename in files:
                    if VERBOSE: print("    Checking %s" % expected_filename)
                    expected_file = os.path.join(root, expected_filename)
                    self.assertTrue(os.path.exists(expected_file), msg="%s does not exist." % expected_file)

                    expected_json = self.load_json_file(expected_file)
                    self.assertNotEqual(expected_json['runId'], runid)

                    output_file_path = self.find_file(output_path, expected_filename)
                    self.assertIsNotNone(output_file_path, msg="Expected output file not found " + expected_filename)
                    output_json = self.load_json_file(output_file_path)

                    self.maxDiff = None

                    self.assertEqual(expected_json, self.ignore_ignored(expected_json, output_json))


if __name__ == "__main__":
    unittest.main()
