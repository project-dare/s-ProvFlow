import unittest
import sys
import os
import json
from pymongo import MongoClient
import mongomock
from flask import Flask
from flask import request
from flask import Response
from flask_cors import CORS
from flask_testing import TestCase
import xml.etree.ElementTree as ET

sys.path.append('../prov-services')
import provenance
import flask_raas


class TestHelper(unittest.TestCase):
    app = flask_raas.app

    # Initialize provenance store with mock database:
    app.db = provenance.ProvenanceStore('UNITTEST')
    lineage = app.db.db.lineage
    workflow = app.db.db.workflow
    term_summaries = app.db.db.term_summaries

    # Mocks the route:
    mockapp = app.test_client()

    # Get full diff
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        '''
        Populates database with workflow & lineage needed. This method is only run once.
        '''
        cls.lineage.remove({})
        cls.workflow.remove({})

        with open('{}/test_input/database/lineage.json'.format(os.getcwd()), encoding="utf-8") as lineage_file:
            cls.lineage.insert_one(json.load(lineage_file))
        with open('{}/test_input/database/lineage_splitmerge_mult3.json'.format(os.getcwd()), encoding="utf-8") as lineage_file2:
            cls.lineage.insert_one(json.load(lineage_file2))
        with open('{}/test_input/database/lineage_splitmerge_mult1.json'.format(os.getcwd()), encoding="utf-8") as lineage_file4:
            cls.lineage.insert_one(json.load(lineage_file4))
        with open('{}/test_input/database/workflow.json'.format(os.getcwd()), encoding="utf-8") as workflow_file:
            cls.workflow.insert_one(json.load(workflow_file))
        with open('{}/test_input/database/term_summaries.json'.format(os.getcwd()), encoding="utf-8") as summaries_file:
            cls.term_summaries.insert_one(json.load(summaries_file))

    def setUp(self):
        '''
        SetUp is run once for every test.
        '''
        pass

    def test_get_workflowexecutions(self):
        '''
        Route: @app.route("/workflowexecutions", methods=['GET'])
        Used by: Sprov-Viewer on startup as well as search terms.
        Test: Find workflow with matching term/value pairs. Note the 'AND mode' parameter, in order to test multiple terms at once. 
        '''

        response_obj = {
            '_id': 'simulation_abruzzo16000000000000_1467113852419',
            'description': '15 stazioni',
            'startTime': '2016-06-28T11:37Z',
            'system_id': 'simulation_abruzzo16000000000000_1467113852419_2016-06-28-123733',
            'username': 'aspinuso',
            'workflowName': 'SCAI_mpi_SPECFEM_PRODUCTION__2015-03-03-120220_2015-11-19-151723'
        }

        #getWorkflowExecution:
        qstring = ('usernames=aspinuso', 'start=0', 'limit=1000')

        response = self.mockapp.get(
            '/workflowexecutions',  query_string='&'.join(qstring), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))  # byte to json
        self.assertEqual(data['runIds'], [response_obj])

        # getWorkflowExecutionByLineage:
        qstring = ('usernames=aspinuso', 'terms=station', 'expressions=LATB', 'mode=OR',
                   'functionNames=', 'clusters=', 'formats=null', '_dc=', 'page=1', 'start=0', 'limit=1000')

        response = self.mockapp.get(
            '/workflowexecutions',  query_string='&'.join(qstring), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))  # byte to json
        self.assertEqual(data['runIds'], [response_obj])

    def test_post_workflowexecutions_insert(self):
        '''
        Route: @app.route("/workflowexecutions/insert", methods=['POST'])
        Used by: Insert from queue
        '''
        route = '/workflowexecutions/insert'
        response_obj = {'success': True, 'inserts': ['mockId']}

        with open('{}/test_input/database/lineage_insert.json'.format(os.getcwd()), 'rb') as provfile:
            prov_obj = {"prov": provfile.read()}

            response = self.mockapp.post(
                route, data=prov_obj, follow_redirects=True)
            
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_post_workflowexecutions_import(self):
        '''
        Route: @app.route("/workflowexecutions/import", methods=['POST'])
        Used by: Import CWL
        Test: Zip is unpacked, each item is imported successfully. As each item gets a unique id, no assertion on the response object is made.
        '''
        route = '/workflowexecutions/import'
        with open('{}/test_input/specfem/specfem_cwl_prov.zip'.format(os.getcwd()), 'rb') as fp:
            response = self.mockapp.post(route, data=dict(
                    archive=fp,
                    format = "CWLProv",
                    resultLocation = "abc",
                ), follow_redirects=True)

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(type(data).__name__, 'list')

        for item in data:
            self.assertEqual(item['success'], True)

    def test_get_data(self):
        '''
        Route: @app.route("/data", methods=['GET'])
        Used by: Sprov-Viewer - Data Products, Search 
        Parameter: 'wasGeneratedBy', <Str>, the id of the invocation that generated the data
        Test: Find data of PE that matches with parameter and search terms
        '''

        qstring = ['SCAI_mpi_SPECFEM_PRODUCTION__2015-03-03-120220_2015-11-19-151723', 'terms=station',
                   'expressions=LATB', 'functionNames=', 'clusters=', 'mode=OR', '_dc=', 'page=1', 'start=0', 'limit=1000']

        response_obj = {'@graph': [{'errors': '',
                                    'derivationIds': [
                                        {'DerivedFromDatasetID':
                                         'drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106',
                                         'TriggeredByProcessIterationID':
                                             'WatchDirectory-drachenfels-057-29482-0f8eaa2c-3d25-11e6-8d8f-0025907bd106',
                                         'port': 'input'}
                                    ],
                                    'parameters': [{'key': 'stations_file',
                                                    'val':
                                                        '/home/aspinuso/e86c6b13-ac8b-4c42-8dae-29d014038ca2/stations'}],
                                    'endTime': '2016-06-28 11:40:06.717088',
                                    'runId': 'simulation_abruzzo16000000000000_1467113852419',
                                    'startTime': '2016-06-28 11:40:06.706681',
                                    'format': '',
                                    'cluster':
                                        'Specfem3d2Stream-Instance--drachenfels-059-9647-0f8ee74e-3d25-11e6-8705-0025907bceb0',
                                    'component': 'Specfem3d2Stream_1',
                                    'functionName': 'Specfem3d2Stream',
                                    'annotations': [],
                                    'content': [{'network': 'IV',
                                                 'prov:type': 'synthetic-waveform',
                                                 'longitude': 12.96239,
                                                 'sampling_rate': 1000,
                                                 'latitude': 41.4939,
                                                 'station': 'LATB',
                                                 'location': '',
                                                 'starttime': '1969-12-31T23:59:57.999975Z',
                                                 'delta': 0.001,
                                                 'calib': 1,
                                                 'npts': 1000,
                                                 'endtime': '1969-12-31T23:59:58.998975Z',
                                                 'type': 'acceleration',
                                                 'id': '0fa8909a-3d25-11e6-8705-0025907bceb0',
                                                 'channel': 'FXZ'}],
                                    'location': '',
                                    'id': 'drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0',
                                    'port': 'output',
                                    'wasGeneratedBy':
                                        'Specfem3d2Stream-drachenfels-059-9647-0fa704b4-3d25-11e6-8705-0025907bceb0',
                                    'size': 64,
                                    'indexedMeta': [{'val': 'IV',
                                                     'key': 'network'},
                                                    {'val': 'synthetic-waveform',
                                                        'key': 'prov:type'},
                                                    {'val': 12.96239,
                                                        'key': 'longitude'},
                                                    {'val': 1, 'key': 'calib'},
                                                    {'val': 1000, 'key': 'npts'},
                                                    {'val': 'LATB',
                                                        'key': 'station'},
                                                    {'val': '', 'key': 'location'},
                                                    {'val': 0.001, 'key': 'delta'},
                                                    {'val': 41.4939,
                                                        'key': 'latitude'},
                                                    {'val': 1000,
                                                        'key': 'sampling_rate'},
                                                    {'val': 'acceleration',
                                                        'key': 'type'},
                                                    {'val': 'FXZ', 'key': 'channel'}
                                                    ]}],
                        '@context': {
                            's-prov':
                                'https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#',
                            'prov': 'http://www.w3.org/ns/prov-o#', 'oa': 'http://www.w3.org/ns/oa.rdf#',
                            'vcard': 'http://www.w3.org/2006/vcard/ns#', 'provone': 'http://purl.org/provone'},
                        'totalCount': 1
                        }

        response = self.mockapp.get(
            '/data',  query_string='&'.join(qstring), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))  # byte to json
        self.assertEqual(data, response_obj)

    def test_post_data_filterOnAncestor(self):
        '''
        Route: @app.route("/data/filterOnAncestor", methods=['POST'])
        Used by: Sprov-Viewer - Data Products, Filter current
        Test: Status code only
        '''
        ids = ["drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0", "drachenfels-056-14772-12ef9154-3d25-11e6-b736-0025907bd0ea",
               "drachenfels-056-14772-0ff0dddc-3d25-11e6-b736-0025907bd0ea", "drachenfels-056-14772-0fc40514-3d25-11e6-b736-0025907bd0ea"]
        data = {"expressions": "LAV9", "ids": ','.join(
            ids), "level": "100", "terms": "station", "mode": "OR", "rformat": "json"}

        response = self.mockapp.post(
            '/data/filterOnAncestor',  data=data, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_get_terms(self):
        '''
        Route: @app.route("/terms")
        Used by: Sprov-Viewer on startup
        Test: Get list of terms based on username and aggregation level.
        '''
        qstring = 'usernames=aspinuso&aggregationLevel=runId_username&_dc=&page=1&start=0&limit=25'

        response_obj = {'metadata': {}, 'terms': [{'term': 'batch', 'use': 'metadata', 'valuesByType': {'number': {'count': 16, 'max': 2, 'min': 1},'string': {'count': 8}}}, {'term': 'var_index', 'use': 'metadata', 'valuesByType': {'number': {'count': 32, 'max': 1, 'min': 0}}}, {'term': 'variables_number', 'use': 'parameter', 'valuesByType': {'number': {'count': 16, 'max': 2, 'min': 2}}}, {'term': 'batch_size', 'use': 'parameter', 'valuesByType': {'number': {'count': 40, 'max': 2, 'min': 2}}}, {'term': 'threshold', 'use': 'parameter', 'valuesByType': {'number': {'count': 16, 'max': 0, 'min': 0}}}, {'term': 'sampling_rate', 'use': 'parameter', 'valuesByType': {'number': {'count': 32, 'max': 100, 'min': 100}}}]}

        response = self.mockapp.get(
            '/terms', query_string=qstring, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_summaries_workflowexecutions(self):
        '''
        Route: @app.route("/summaries/workflowexecution")
        Used by: Sprov-Viewer - Radial Prov Analysis
        '''
        qstring = 'minidx =0&maxidx=10&level=prospective&groupby=actedOnBehalfOf&runId=simulation_abruzzo16000000000000_1467113852419'
        response_obj = [{'time': '2016-06-28 11:40:06.706681', 'name': {'actedOnBehalfOf': 'Specfem3d2Stream_1', 'mapping': 'mpi', 'runId': 'simulation_abruzzo16000000000000_1467113852419'}, 'connlist': []}]

        response = self.mockapp.get(
            '/summaries/workflowexecution', query_string=qstring, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_workflowexecutions_runid_showactivity(self):
        '''
        Route: @app.route("/workflowexecutions/<runid>/showactivity")
        Used by: Sprov-Viewer - Workflow Runs
        '''
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        route = '/workflowexecutions/{}/showactivity'.format(runId)
        qstring = 'level=instance&start=0&limit=25'
        response_obj = {'@graph': [{'s-prov:lastEventTime': '2016-06-28 11:40:06.717088', 's-prov:message': '', 's-prov:worker': 'drachen-000', 'prov:actedOnBehalfOf': {'@type': 's-prov:Component', '@id': 'Specfem3d2Stream_1'}, 's-prov:generatedWithImmediateAccess': False, 's-prov:generatedWithLocation': False, 's-prov:qualifiedChange': [], 's-prov:dataCount': 1,
                                    '@id': 'Specfem3d2Stream-Instance--drachenfels-059-9647-0f8ee74e-3d25-11e6-8705-0025907bceb0', '@type': 's-prov:ComponentInstance'}], '@context': {'s-prov': 'https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#', 'prov': 'http://www.w3.org/ns/prov-o#', 'oa': 'http://www.w3.org/ns/oa.rdf#', 'vcard': 'http://www.w3.org/2006/vcard/ns#', 'provone': 'http://purl.org/provone'}, 'totalCount': 1}

        response = self.mockapp.get(
            route, query_string=qstring, follow_redirects=True)

        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_workflowexecutions_runid(self):
        '''
        Route: @app.route("/workflowexecutions/<runid>")
        Used by: Sprov-Viewer - View Inputs
        '''
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        route = '/workflowexecutions/{}'.format(runId)
        response_obj = {'_id': 'simulation_abruzzo16000000000000_1467113852419', 'username': 'aspinuso', 'workflowId': '258', 'resource': 'draco-ext.scai.fraunhofer.de:2119', 'workflowName': 'SCAI_mpi_SPECFEM_PRODUCTION__2015-03-03-120220_2015-11-19-151723', 'resourceType': 'gt5', 'type': 'workflow_run', 'prov:type': 'simulation', 'grid': 'SCAI_Cluster2', 'queue': 'jobmanager-pbs', 'job0bin': '', 'system_id': 'simulation_abruzzo16000000000000_1467113852419_2016-06-28-123733', 'startTime': '2016-06-28T11:37Z', 'input': [{'url': 'https://portal.verce.eu/documents/10180/11604/stations_abruzzi01424783407330', 'mime-type': 'application/xml', 'name': 'stations'}, {'url': 'https://portal.verce.eu/documents/10180/11604/events_simulation_abruzzo160000_1443707335437', 'mime-type': 'application/xml', 'name': 'quakeml'}, {'url': 'https://portal.verce.eu/documents/10180/11644/SPECFEM3D_CARTESIAN_202_DEV_simulation_abruzzo16000000000000_1467113852419.json', 'mime-type': 'application/json', 'name': 'solverconf'}, {'url': 'https://portal.verce.eu/documents/10180/11630/simulation_abruzzo16000000000000_1467113852419.zip', 'mime-type': 'application/zip', 'name': 'vercepes'}], 'job0binModified': '1970-01-01T01:00+0100', 'description': '15 stazioni'}

        response = self.mockapp.get(
            route, query_string='', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_workflowexecutions_runid_export(self):
        '''
        Route: @app.route("/workflowexecutions/<run_id>/export")
        Used by: Sprov-Viewer - Get W3C Prov
        Test: Test that json/xml documents are returned. Assertion on key attributes instead of full response due to their length.
        '''
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        route = '/workflowexecutions/{}/export'.format(runId)

        # JSON:
        jsonstring = 'format=json&rdfout=nt&creator=aspinuso'
        response = self.mockapp.get(
            route, query_string=jsonstring, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(type(response.data), 'json')

        try:
            data = json.loads(response.data.decode('utf-8'))
            self.assertEqual(data['agent'], {'uuid:ag_aspinuso': {'dcterms:creator': 'aspinuso'}})
            self.assertTrue(data["prefix"])
            self.assertTrue(data["entity"])
            self.assertTrue(data["agent"])
            self.assertTrue(data["wasAttributedTo"])
            self.assertTrue(data["bundle"])
        except:
            self.fail('No workflow by id or not a JSON')


        # XML:
        xmlstring = 'format=xml&rdfout=nt&creator=aspinuso'
        response = self.mockapp.get(
            route, query_string=xmlstring, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        tree = ET.fromstring(response.data.decode('utf-8'))
        childs = [
                '{http://www.w3.org/ns/prov#}entity',
                '{http://www.w3.org/ns/prov#}agent',
                '{http://www.w3.org/ns/prov#}wasAttributedTo',
                '{http://www.w3.org/ns/prov#}bundleContent'
            ]

        for child in tree:
            self.assertIn(child.tag, childs)

    def test_post_workflowexecutions_runid_edit(self):
        '''
        Route: @app.route("/workflowexecutions/<run_id>/edit")
        Used by: Sprov-Viewer - Workflow Runs
        '''
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        route = '/workflowexecutions/{}/edit'.format(runId)
        doc_obj = {"doc": json.dumps({'description': 'abc'})}
        response_obj = {'success': True,
                       'edit': 'simulation_abruzzo16000000000000_1467113852419'}

        response = self.mockapp.post(
            route, data=doc_obj, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_post_workflowexecutions_runid_delete(self):
        '''
        Route: @app.route("/workflowexecutions/<run_id>/delete")
        Used by: Sprov-Viewer - Workflow Runs
        '''
        runId = 'workflowToDelete'
        route = '/workflowexecutions/{}/delete'.format(runId)
        response_obj = {'success': True, 'delete': 'workflowToDelete'}

        with open('{}/test_input/database/workflow_todelete.json'.format(os.getcwd()), encoding="utf-8") as wftd:
            self.workflow.insert_one(json.load(wftd))

        response = self.mockapp.post(route, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_data_dataid(self):
        '''
        Route: @app.route("/data/<data_id>", methods=['GET'])
        Used by: Sprov-Viewer - Data Dependency Graph
        '''
        dataId = 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5'
        route = '/data/{}'.format(dataId)
        response_obj = {'@graph': [{'content': [{}], 'id': 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5', 'format': '', 'location': '', 'annotations': [], 'port': 'output', 'size': 216, 'indexedMeta': [], 'wasGeneratedBy': 'PE_mult-fstriewski-Precision-5530-28533-f946e902-68fd-11ea-835f-5c879cf9c3b5', 'parameters': [], 'startTime': '2020-03-18 09:51:00.775655', 'endTime': '2020-03-18 09:51:00.775770', 'runId': 'fstriewski-Precision-5530-28533-f946e8ca-68fd-11ea-835f-5c879cf9c3b5', 'errors': '', 'derivationIds': [{'port': 'input', 'DerivedFromDatasetID':                                                                                         'fstriewski-Precision-5530-28533-f946e8e4-68fd-11ea-835f-5c879cf9c3b5', 'TriggeredByProcessIterationID': 'splitPE-fstriewski-Precision-5530-28533-f946e8dd-68fd-11ea-835f-5c879cf9c3b5', 'prov_cluster': 'dare:DataSplit', 'iterationIndex': 1}], 'cluster': 'PE_mult3', 'component': 'PE_mult3', 'functionName': 'PE_mult'}], '@context': {'s-prov': 'https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#', 'prov': 'http://www.w3.org/ns/prov-o#', 'oa': 'http://www.w3.org/ns/oa.rdf#', 'vcard': 'http://www.w3.org/2006/vcard/ns#', 'provone': 'http://purl.org/provone'}, 'totalCount': 2}

        response = self.mockapp.get(
            route, query_string='', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_data_dataid_wasDerivedFrom(self):
        '''
        Route: @app.route("/data/<data_id>/wasDerivedFrom?", methods=['GET'])
        Used by: Sprov-Viewer - Data Dependency Graph
        '''
        dataId = 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5'
        route = '/data/{}/wasDerivedFrom'.format(dataId)
        response_obj = {'s-prov:Data': {'port': 'output', 'size': 216, '@id': 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5', 'prov:atLocation': '', 'prov:hadMember': [{'@type': 's-prov:DataGranule'}], 'prov:Derivation': [], 'prov:wasGeneratedBy': {'s-prov:Invocation': {'@id': 'PE_mult-fstriewski-Precision-5530-28533-f946e902-68fd-11ea-835f-5c879cf9c3b5'}, 's-prov:WFExecution': {'@id': 'fstriewski-Precision-5530-28533-f946e8ca-68fd-11ea-835f-5c879cf9c3b5'}}, 'prov:wasAttributedTo': {'@id': 'PE_mult3', '@type': 's-prov:Component'}}, '@context': {'s-prov': 'https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#', 'prov': 'http://www.w3.org/ns/prov-o#', 'oa': 'http://www.w3.org/ns/oa.rdf#', 'vcard': 'http://www.w3.org/2006/vcard/ns#', 'provone': 'http://purl.org/provone'}}

        response = self.mockapp.get(
            route, query_string='level=1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_data_dataid_derivedData(self):
        '''
        Route: @app.route("/data/<data_id>/derivedData?", methods=['GET'])
        Used by: Sprov-Viewer - Data Dependency Graph
        '''
        dataId = 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5'
        route = '/data/{}/derivedData'.format(dataId)
        response_obj = {'runId': 'fstriewski-Precision-5530-28533-f946e8ca-68fd-11ea-835f-5c879cf9c3b5', 'derivationIds': [{'port': 'input', 'DerivedFromDatasetID': 'fstriewski-Precision-5530-28533-f946e8e4-68fd-11ea-835f-5c879cf9c3b5', 'TriggeredByProcessIterationID': 'splitPE-fstriewski-Precision-5530-28533-f946e8dd-68fd-11ea-835f-5c879cf9c3b5', 'prov_cluster': 'dare:DataSplit', 'iterationIndex': 1}], 'streams': [{'port': 'output', 'location': ''}], '_id': 'PE_mult3_write_fstriewski-Precision-5530-28533-f946e905-68fd-11ea-835f-5c879cf9c3b5', 'dataId': 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5', 'derivedData': []}

        response = self.mockapp.get(
            route, query_string='level=1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))
        self.assertEqual(data, response_obj)

    def test_get_data_dataid_export(self):
        '''
        Route: @app.route("/data/<data_id>/export")
        Used by: Sprov-Viewer - Data products – Download provenance
        Test: Test that json/xml documents are returned. Assertion on key attributes instead of full response due to their length.
        '''
        data_id = 'fstriewski-Precision-5530-28533-f946e8f7-68fd-11ea-835f-5c879cf9c3b5'
        route = '/data/{}/export'.format(data_id)

        # JSON:
        jsonstring = 'format=json&rdfout=nt&creator=aspinuso&level=2'
        response = self.mockapp.get(
            route, query_string=jsonstring, follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))

        try:
            self.assertEqual(data['agent'], {'uuid:ag_aspinuso': {'dcterms:creator': 'aspinuso'}})
            self.assertTrue(data["prefix"])
            self.assertTrue(data["entity"])
            self.assertTrue(data["agent"])
            self.assertTrue(data["wasAttributedTo"])
            self.assertTrue(data["bundle"])
        except:
            self.fail('No matching id or not a JSON')

    def tearDown(self):
        '''
        TearDown is run once for every test.
        '''
        pass

if __name__ == "__main__":
    unittest.main()
