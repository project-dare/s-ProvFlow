import unittest
import sys

sys.path.append('../prov-services')
import helper


class TestHelper(unittest.TestCase):

    def test_parse_values(self):
        parsed_string = helper.parse_values('abc')
        self.assertTrue(type(parsed_string) == str)
        self.assertTrue(parsed_string == 'abc')

        parsed_int = helper.parse_values('10')
        self.assertTrue(type(parsed_int) == int)
        self.assertTrue(parsed_int == 10)

        parsed_float = helper.parse_values('1.00001')
        self.assertTrue(type(parsed_float) == float)
        self.assertTrue(parsed_float == 1.00001)


if __name__ == "__main__":
    unittest.main()
