import unittest
from unittest.mock import MagicMock, call
import mongomock
import json
import os

import sys
sys.path.append('../prov-services')
import provenance

class FilterOnAncestors(unittest.TestCase):

    db = provenance.ProvenanceStore('UNITTEST')

    def test_singleValue(self):
        ids=["drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0"]
        self.db.lineage.find_one = MagicMock()
        self.db.lineage.find_one.return_value = \
            {'derivationIds': [
                {'DerivedFromDatasetID': 'drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106',
                 'TriggeredByProcessIterationID': 'WatchDirectory-drachenfels-057-29482-0f8eaa2c-3d25-11e6-8d8f-0025907bd106',
                 'port': 'input'}
            ], 'startTime': '2016-06-28 11:40:06.706681', 'runId': 'simulation_abruzzo16000000000000_1467113852419',
                '_id': 'Specfem3d2Stream_write_drachenfels-059-9647-0fa89b26-3d25-11e6-8705-0025907bceb0'}

        ## Do the call on the ProvenanceStore
        filteredIds = self.db.filterOnAncestorsValuesRange(ids, ["station"], ["LAV9"])

        assert filteredIds == ["drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0"]

        assert self.db.lineage.find_one.call_count == 2
        assert self.db.lineage.find_one.call_args_list[0] == call(
            {'streams.id': 'drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0'},
            {'derivationIds': 1, 'startTime': 1,'runId': 1}
        )
        assert self.db.lineage.find_one.call_args_list[1] == call(
            {'streams': {
                '$elemMatch':
                    {'id': {'$in': ['drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106']},
                     '$or': [{'indexedMeta': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LAV9'}}}}]}}},
            { '_id': 1 }
        )

    def test_noneValue(self):
        ids=["drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0"]
        self.db.lineage.find_one = MagicMock()
        self.db.lineage.find_one.return_value = None

        ## Do the call on the ProvenanceStore
        filteredIds = self.db.filterOnAncestorsValuesRange(ids, ["station",], ["LAV9"])

        assert filteredIds == []

        assert self.db.lineage.find_one.call_count == 1

    @unittest.mock.patch.object(mongomock.collection.Collection, "find_one",
                                new_callable=lambda: FilterOnAncestors.__find_one_mock)
    def test_nextLevelQuery(self, collection):
        ids=["drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0"]
        ## All we need to know is if the query was correct. So find() can return None, we check later
        ## if find() was called with the right arguments.
        self.db.lineage.find = MagicMock()
        self.db.lineage.find.return_value = None

        filteredIds = self.db.filterOnAncestorsValuesRange(ids, ["station"], ["LAV9"])

        assert self.db.lineage.find.call_args_list[0] == call(
            {'streams':
                 {'$elemMatch': {'id': {'$in': ['drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106']}}}},
            { 'derivationIds': 1 }
        )

    def __find_one_mock(self, filter, projection):
        if filter == {'streams.id': 'drachenfels-059-9647-0fa89838-3d25-11e6-8705-0025907bceb0'}:
            return {
                'derivationIds': [
                    {'DerivedFromDatasetID': 'drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106',
                     'TriggeredByProcessIterationID': 'WatchDirectory-drachenfels-057-29482-0f8eaa2c-3d25-11e6-8d8f-0025907bd106',
                     'port': 'input'}
                ],
                'startTime': '2016-06-28 11:40:06.706681', 'runId': 'simulation_abruzzo16000000000000_1467113852419',
                '_id': 'Specfem3d2Stream_write_drachenfels-059-9647-0fa89b26-3d25-11e6-8705-0025907bceb0'}
        if filter == {'streams': {
            '$elemMatch':
                {'id': {'$in': ['drachenfels-057-29482-0f966488-3d25-11e6-8d8f-0025907bd106']},
                 '$or': [{'indexedMeta': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LAV9'}}}}]}}}:
            return None

class WorkflowExecutions(unittest.TestCase):

    db = provenance.ProvenanceStore('UNITTEST')

    def test_getWorkflowExecution(self):
        '''
        Method: provenance.py/getWorkflowExecution(self, start, limit, usernames)
        Called by: flask_raas.py/get_workflowexecutions
        '''
        query = {'username': {'$in': ['aspinuso']}}

        self.db.workflow.find = MagicMock()
        self.db.getWorkflowExecution(0,1,['aspinuso'])

        assert self.db.workflow.find.call_count == 1
        assert self.db.workflow.find.call_args_list[0] == call(query,{
                "startTime":1,
                "system_id":1,
                "description":1,
                "workflowName":1,
                "username":1
            })

        self.db.workflow.count = MagicMock()
        self.db.getWorkflowExecution(0,1,['aspinuso'])

        assert self.db.workflow.count.call_count == 1
        assert self.db.workflow.count.call_args_list[0] == call(query)

    def test_getWorkflowExecutionByLineage(self):
        '''
        Method: getWorkflowExecutionByLineage(self, start, limit, usernames, associatedWith, implementations, keylist, expressions, mode = 'OR', types=None,formats = None,clusters=None)
        Called by: flask_raas.py/get_workflowexecutions
        '''

        aggregate_match = {'username': {'$in': ['aspinuso']}, '$or': [{'streams': {'$elemMatch': {'indexedMeta': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LATB'}}}}}}, {'parameters': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LATB'}}}}]}
        and_query = [{'$or': [{'indexedMeta': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LATB'}}}}, {'parameters': {'$elemMatch': {'key': 'station', 'val': {'$eq': 'LATB'}}}}]}]

        or_pipeline = [
            {
                '$match':  aggregate_match
            },
            {
                '$group': {
                    '_id':'$runId'
                }
            }
        ]

        and_pipeline = [
            {
                '$match': aggregate_match,
            },
            {
                '$unwind': '$streams'
            },
            {
                '$unwind': {
                    'path': '$streams.indexedMeta',
                    'preserveNullAndEmptyArrays': True
                }
            },
            {
                '$unwind': {
                    'path': '$parameters',
                    'preserveNullAndEmptyArrays': True
                }
            },
            {
                '$group': {
                    '_id':'$runId',
                    'indexedMeta': {
                        '$addToSet': "$streams.indexedMeta"
                    },
                    'parameters': {
                        '$addToSet': "$parameters"
                    }
                },
            },
            {
                '$match': {
                    '$and': and_query
                }
            }
        ]

        # Used for OR and AND:
        self.db.lineage.aggregate = MagicMock()

        # OR mode
        or_mode = self.db.getWorkflowExecutionByLineage(0,1,['aspinuso'], None, None, ['station'], ['LATB'], 'OR')
        assert self.db.lineage.aggregate.call_count == 1
        assert self.db.lineage.aggregate.call_args_list[0] == call(pipeline = or_pipeline)

        # AND mode
        and_mode = self.db.getWorkflowExecutionByLineage(0,1,['aspinuso'], None, None, ['station'], ['LATB'], 'AND')
        assert self.db.lineage.aggregate.call_count == 2
        assert self.db.lineage.aggregate.call_args_list[1] == call(pipeline = and_pipeline)

        self.db.workflow.find = MagicMock()
        self.db.getWorkflowExecutionByLineage(0,1,['aspinuso'], None, None, ['station'], ['LATB'], 'OR')
        assert self.db.workflow.find.call_count == 1
        assert self.db.workflow.find.call_args_list[0] == call(
            {"_id":{
                "$in":[]
                },
            },{
            "startTime":1,
            "system_id":1,
            "description":1,
            "workflowName":1,
            "username":1
            })

        self.db.addLDContext = MagicMock()
        self.db.addLDContext.return_value = {'runIds': [], 'totalCount': 0, '@context': {'s-prov': 'https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#', 'prov': 'http://www.w3.org/ns/prov-o#', 'oa': 'http://www.w3.org/ns/oa.rdf#', 'vcard': 'http://www.w3.org/2006/vcard/ns#', 'provone': 'http://purl.org/provone'}}

        context = self.db.addLDContext({})
        assert self.db.addLDContext.call_args_list[0] == call({})
        assert self.db.addLDContext.call_count == 1
        assert self.db.addLDContext.return_value == context

class WorkflowExecutions_Insert(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_insertData(self):
        self.db.lineage.insert = MagicMock()

        with open('{}/test_input/database/lineage_insert.json'.format(os.getcwd()), 'rb') as provfile:
            json_file = provfile.read()
            deserialized_prov = json.loads(json_file)
            self.db.insertData(deserialized_prov)

        assert self.db.lineage.insert.call_count == 1

        args, kwargs = self.db.lineage.insert.call_args
        del args[0]["insertedAt"]

        self.assertEqual(self.db.lineage.insert.call_args_list[0],call({'_id': 'mockId', 'iterationId': 'mock_iterationId', 'actedOnBehalfOf': 'mergePE4', 'iterationIndex': 3, 'instanceId': 'mock_instanceId', 'annotations': {}, 'stateful': True, 'feedbackIteration': False, 'worker': 'mock_worker', 'parameters': [], 'errors': '', 'pid': '28533', 'derivationIds': [{'port': 'input0', 'DerivedFromDatasetID': 'mock', 'TriggeredByProcessIterationID': 'mock', 'prov_cluster': 'PE_mult1', 'iterationIndex': 1}], 'name': 'mergePE', 'runId': 'mock_runId','username': 'aspinuso', 'startTime': '2020-03-18 09:51:00.792818', 'endTime': '2020-03-18 09:51:00.792891', 'type': 'lineage', 'streams': [{'content': [{'output': '[2, 4, 6, 8, 2, 4, 6, 8, 10, 12, 14, 8]'}], 'id': 'mockId', 'format': '', 'location': '', 'annotations': [], 'port': 'output', 'size': 364, 'indexedMeta': [{'key': 'output', 'val': '[2, 4, 6, 8, 2, 4, 6, 8, 10, 12, 14, 8]'}]}], 'mapping': 'simple', 'prov_cluster': 'dare:DataMerge'}))

class WorkflowExecutions_Import(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_insertData(self):
        self.maxDiff = None
        self.db.lineage.insert = MagicMock(return_value=None)
        self.db.workflow.update_one = MagicMock()

        with open('{}/test_input/specfem/metadata/provenance/single_file.json'.format(os.getcwd()), 'rb') as provfile:
            json_file = provfile.read()
            deserialized_prov = json.loads(json_file)
            self.db.insertData(deserialized_prov)

            assert self.db.lineage.insert.call_count == 1

            args, kwargs = self.db.lineage.insert.call_args

            self.assertEqual(args[0]["_id"], deserialized_prov["_id"])
            self.assertEqual(args[0]["runId"], deserialized_prov["runId"])
            self.assertEqual(args[0]["username"], deserialized_prov["username"])
            self.assertEqual(len(args[0]["streams"]), len(deserialized_prov["streams"]))

class WorkflowExecutions_RunId(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    db.lineage.remove = MagicMock()
    db.workflow.remove = MagicMock()
    db.workflow.find_one = MagicMock()

    def test_getRunInfo(self):
        path = '/abc'
        runInfo = self.db.getRunInfo(path)

        assert self.db.workflow.find_one.call_count == 2
        assert self.db.workflow.find_one.call_args_list[1] == call({"_id": path})
        assert self.db.workflow.find_one.return_value == runInfo

    def test_deleteRun(self):
        id = 'abc'
        self.db.deleteRun(id)

        assert self.db.workflow.find_one.call_count == 1
        assert self.db.lineage.remove.call_count == 1
        assert self.db.workflow.remove.call_count == 1

        assert self.db.workflow.find_one.call_args_list[0] == call({"_id": id})
        assert self.db.lineage.remove.call_args_list[0] == call({"runId": id})
        assert self.db.workflow.remove.call_args_list[0] == call({"_id": id})

class WorkflowExecutions_RunId_ShowActivity(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_getMonitoring(self):
        self.db.lineage.aggregate = MagicMock()
        self.db.addLDContext = MagicMock()
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        start = 0
        limit = 25

        self.db.getMonitoring(runId, 'instance', start, limit)

        assert self.db.lineage.aggregate.call_count == 2
        assert self.db.lineage.aggregate.call_args_list[0] == call(pipeline=[{'$match': {'runId': runId}}, {'$unwind': '$streams'}, {'$group': {'_id': '$instanceId', 's-prov:lastEventTime': {'$max': '$endTime'}, 's-prov:message': {'$push': '$errors'}, 's-prov:worker': {'$first': '$worker'}, 'prov:actedOnBehalfOf': {'$first': '$actedOnBehalfOf'}, 's-prov:generatedWithImmediateAccess': {'$push': '$streams.con:immediateAccess'}, 's-prov:generatedWithLocation': {'$push': '$streams.location'}, 's-prov:qualifiedChange': {'$push': '$s-prov:qualifiedChange'}, 's-prov:dataCount': {'$push': '$streams.id'}}}, {'$sort': {'s-prov:lastEventTime': -1}}, {'$skip': start}, {'$limit': limit}])
        assert self.db.lineage.aggregate.call_args_list[1] == call(pipeline=[{'$match': {'runId': runId}}, {'$group': {'_id': '$instanceId'}}, {"$count": 'instanceId'}])

        assert self.db.addLDContext.call_count == 1

class WorkflowExecutions_RunId_Edit(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_editRun(self):
        self.db.workflow.update = MagicMock()
        runId = 'simulation_abruzzo16000000000000_1467113852419'
        doc = {"test": "abc"}

        self.db.editRun(runId, doc)
        assert self.db.workflow.update.call_count == 1
        assert self.db.workflow.update.call_args_list[0] == call({"_id": runId}, {'$set': doc})

class WorkflowExecutions_RunId_Delete(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    db.lineage.remove = MagicMock()
    db.workflow.remove = MagicMock()
    db.workflow.find_one = MagicMock()

    def test_deleteRun(self):
        id = 'abc'
        self.db.deleteRun(id)

        assert self.db.workflow.find_one.call_count == 1
        assert self.db.lineage.remove.call_count == 1
        assert self.db.workflow.remove.call_count == 1

        assert self.db.workflow.find_one.call_args_list[0] == call({"_id": id})
        assert self.db.lineage.remove.call_args_list[0] == call({"runId": id})
        assert self.db.workflow.remove.call_args_list[0] == call({"_id": id})

class WorkflowExecutions_RunId_Export(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_exportRunProvenance(self):
        self.db.workflow.find_one = MagicMock()
        self.db.lineage.find = MagicMock()
        runId = 'abc'
        creator = 'mockUser'

        self.db.exportRunProvenance(runId, creator, mode='unittest')

        assert self.db.workflow.find_one.call_count == 1
        assert self.db.lineage.find.call_count == 1

        assert self.db.workflow.find_one.call_args_list[0] == call({"_id": runId})

class Data(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_getData(self):
        self.db.lineage.find = MagicMock()
        self.db.lineage.count = MagicMock()
        self.db.lineage.aggregate = MagicMock()

        self.db.getData(0, 1)

        assert self.db.lineage.find.call_count == 2
        assert self.db.lineage.find.call_args_list[0] == call({}, {'iterationId': 1, 'prov_cluster': 1, 'actedOnBehalfOf': 1, 'name': 1, 'runId': 1, 'streams': 1, 'parameters': 1, 'startTime': 1, 'endTime': 1, 'errors': 1, 'derivationIds': 1})
        assert self.db.lineage.find.call_args_list[1] == call({}, {'streams': 1})

class Data_DataId_WasDerivedFrom(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_getTrace(self):
        self.db.lineage.find = MagicMock()
        dataId = 'mockId'
        level = '1'

        self.db.getTrace(dataId, int(level))

        assert self.db.lineage.find.call_count == 1
        assert self.db.lineage.find.call_args_list[0] == call({'streams.id': {'$in': [dataId]}}, {'streams.content': 1, 'streams.id': 1, 'streams.port': 1,'streams.size': 1, 'iterationId': 1, 'iterationIndex': 1, 'runId': 1, 'streams.location': 1, 'actedOnBehalfOf': 1, 'derivationIds': 1, '_id': 0})

    def test_addLDContext(self):
        self.db.addLDContext = MagicMock()
        self.db.addLDContext.return_value = {"@context": {"s-prov": "https://raw.githubusercontent.com/KNMI/s-provenance/master/resources/s-prov-o.owl#","prov": "http://www.w3.org/ns/prov-o#","oa": "http://www.w3.org/ns/oa.rdf#","vcard": "http://www.w3.org/2006/vcard/ns#","provone": "http://purl.org/provone"}}

        context = self.db.addLDContext({})

        assert self.db.addLDContext.call_count == 1
        assert self.db.addLDContext.call_args_list[0] == call({})
        assert self.db.addLDContext.return_value == context

class Data_DataId_DerivedData(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_getDerivedDataTrace(self):
        self.db.lineage.find_one = MagicMock()
        self.db.lineage.find = MagicMock()
        dataId = 'mockId'
        level = '1'

        self.db.getDerivedDataTrace(dataId, int(level))

        assert self.db.lineage.find_one.call_count == 1
        assert self.db.lineage.find_one.call_args_list[0] == call({"streams.id": dataId}, {"runId": 1, "derivationIds": 1, 'streams.port': 1, 'streams.location': 1})

        assert self.db.lineage.find.call_count == 1
        assert self.db.lineage.find.call_args_list[0] == call({"derivationIds": {'$elemMatch': {"DerivedFromDatasetID": dataId}}}, {"runId": 1, "streams": 1})

class Data_DataId(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_getData(self):
        self.db.lineage.find = MagicMock()
        self.db.lineage.aggregate = MagicMock()
        dataId = 'mockId'

        self.db.getData(0,1,dataId)

        assert self.db.lineage.find.call_count == 2
        assert self.db.lineage.find.call_args_list[0] == call({'$and': [{'$or': [{'name': {'$in': ['mockId']}}]}]}, {
        'iterationId': 1, 'prov_cluster': 1, 'actedOnBehalfOf': 1, 'name': 1, 'runId': 1, 'streams': 1, 'parameters': 1, 'startTime': 1, 'endTime': 1, 'errors': 1, 'derivationIds': 1})
        assert self.db.lineage.find.call_args_list[1] == call({'$and': [{'$or': [{'name': {'$in': ['mockId']}}]}]}, {'streams': 1})

class Data_DataId_Export(unittest.TestCase):
    db = provenance.ProvenanceStore('UNITTEST')

    def test_exportDataProvenance(self):
        self.db.lineage.find = MagicMock()
        self.db.workflow.find = MagicMock()
        dataId = 'mockId'

        self.db.exportDataProvenance(dataId, 'aspinuso', level=1, mode='unittest')

        assert self.db.workflow.find.call_count == 1
        assert self.db.lineage.find.call_count == 2
        assert self.db.lineage.find.call_args_list[0] == call({'runId': dataId})


class WorkflowExistsTestCase(unittest.TestCase):

    db = provenance.ProvenanceStore('UNITTEST')

    @unittest.mock.patch.object(mongomock.collection.Collection, "find")
    def test_workflowNotExists(self, mock_find):
        mock_cursor = mock_find.return_value
        mock_cursor.count.return_value = 0
        self.assertFalse(self.db.workflowExists("random-run-id"))
        self.assertTrue(mock_find.called_with(
            {'runId':{'$exists':'true', '$eq': 'random-run-id'}}, limit=1))

    @unittest.mock.patch.object(mongomock.collection.Collection, "find")
    def test_workflowExists(self, mock_find):
        mock_cursor = mock_find.return_value
        mock_cursor.count.return_value = 1
        self.assertTrue(self.db.workflowExists("random-run-id"))
        self.assertTrue(mock_find.called_with(
            {'runId':{'$exists':'true', '$eq': 'random-run-id'}}, limit=1))


if __name__ == '__main__':
    unittest.main()
