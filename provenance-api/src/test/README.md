# Available Tests 

Tests of this folder can be run manually or automatically via the CI/CD pipeline. 

A. Run test via pipeline:
The pipeline, defined in s-ProvFlow/.gitlab-ci.yml, has a unit test stage that will excute the unit tests one by one. Note that this is done by installing the requirements.txt as well as the 'coverage' tool before passing the test folder path to the source property of coverage. Coverage then executes all tests automtically. 
For now the pipeline is only run when commiting in development branches and not on master. Adjust the 'except' and 'only' properties to change the behaviour.

B. Run tests locally:
Install the requirements.txt from s-ProvFlow. After that you can either run the tests manually one by one (e.g. `python test_helper.py`) or install coverage and run it with the command as specified in .gitlab-ci.yml. Make sure to point 'source' to the test folder.


## A. Scoped unit tests:

### 1. Test for cwlprov_2_sprov 

cwlprov_2_sprov maps the provenance output of a cwl run (with --provenance) to Sprov format.

NOTE: the test is written in python 3; python 2 does not work. 

##### Test 

Two testcases are implemented: 
- SpecFem3d
- Download.

The provenance created by the two runs are located in ./input. The reference outputs are located under ./expected

The expected files can contain "\_\_ignored\_\_" as value. This value will be ignored in comparison with the generated output.

The SpecFem3d is based on specfem on cwl provenance run (https://gitlab.com/project-dare/WP6_EPOS/tree/master/specfem3d/specfem3d_test_input_cwl)
inside container rosafilgueira/specfem3d_mpi:version1
Run on January 14, 2020


##### Run the test
Simple run the test_cwlprov_2_sprov.py by:
```aidl
python test_cwlprov_2_sprov.py
``` 
we need the environment set to python 3, with the libraries:
- uuid
- json
- unittest

### 2. Unit tests for extended expressions
##### Background:
EUT-399 introduced expressions that can be used to query Mongo DB. Before the change, only ranges (specified by a min and max field) were allowed in sprov-viewer. After the change, open ranges (from a value to the min or max value in the data collection), comma separated lists of values as well as single values can be used to retrieve runs from Mongo DB. The syntax is as follows:

| Type |  Allowed Data Type |Syntax (Expression) | Query |
| ------ | ------ | ------ | ------ |
| Range | String, Float, Int | 1...3 | '$gte' and '$lte'|
| Range from value to max | String, Float, Int | 1... | '$gte' |
| Range from min to value | String, Float, Int | ...3 | '$lte' |
| List | String, Float, Int |  A,B,C | '$in' |
| Single value | String, Float, Int, Boolean |  True | '$eq' |

##### Changes to the API
A new helper method was introduced (../helper.py) that parses the user input to select the correct data type query. The unit test tests both parses - parsing strings into numerical data types (int, float) where necessary, as well as the actual 'translator' in accordance to the table above.

##### Run the test manually:
In terminal: cd into the location of the test (`sprov-services/test/`) run the test with `python test_helper.py` and `python test_getValuePairsExtended.py`.

### 3. Unit tests asserting database queries

##### Background:
These tests are scoped to queries made to the database instead of testing a full flow. Assertions are made on the query and the arguments used, instead of the result (returned from a mock database). Such tests are used for more involved functionality, like done by the filterOnAncestor endpoint.


##### Run the test manually:
In terminal: cd into the location of the test (`sprov-services/test/`) run the test with `python test_provstore.py`.

## B. Endpoint tests

##### Background:
Endpoint tests should take a query string (as it is coming from the viewer) and assert on 
- Status code
- Response object (to the viewer)

For this reason both mongo db and ProvenanceStore have to be mocked: - Mongo DB can be mocked by mongomock. Initialize ProvenanceStore with the url "UNITTEST" to make request against the fake database.
- Flasks own test_client can be used to mock out the Flask functionality. See the setUp method of TestHelper where the client is defined. Use the test client to assert on status codes.

##### Changes to the API
ProvenanceStore is initialized from gunicorn/docker, that passes a url to be used for the database. The ProvenanceStore class (in provenance.py) was changed to use a mock database if the url "UNITTEST" is passed from a test.

##### Run the test manually:
In terminal: cd into the location of the test (`sprov-services/test/`) run the test with `python test_endpoints.py`.


## Generate a coverage report:
A coverage report is generated automatically when running the pipeline. To manually generate a coverage report in the terminal, do the following steps:
- install dependencies from requirements.txt, if necessary. 
- cd into the test folder (src/test)
- run `coverage run --source=../. --omit=test* --rcfile=.coveragerc --branch -m unittest discover` to run all tests at once. -- source points to the folder containing the scripts (src), --omit specifies which files to exclude from being evaluated (files starting with "test") --rcfile points to the config file --branch specifies that branches shall be tested.
- To display results in the terminal type `coverage report` 
- To generate a coverage html report type `coverage html --directory=<foldername/>` where foldername points to a directory.
