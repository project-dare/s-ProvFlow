import unittest
import sys
sys.path.append('../prov-services')

import helper


class TestHelper(unittest.TestCase):

    def test_getKeyValuePairsExtended_Range(self):

        # Range with Min Max
        keys = ['key1', 'key2']
        values = ['a...b', '1...3']
        target = [{
            'key': 'key1',
            'val': {
                '$gte': 'a',
                '$lte': 'b'
            }
        },
            {
            'key': 'key2',
            'val': {
                '$gte': 1,
                '$lte': 3
            }
        }]

        key_val_match = helper.getKeyValuePairsExtended(keys, values)
        self.assertEqual(target, key_val_match)

        # Open Range
        values = ['...1.0', '9...']
        target = [{
            'key': 'key1',
            'val': {
                '$lte': 1.0
            }
        },
            {
            'key': 'key2',
            'val': {
                '$gte': 9
            }
        }]

        key_val_match = helper.getKeyValuePairsExtended(keys, values)
        self.assertEqual(target, key_val_match)

    def test_getKeyValuePairsExtended_List(self):
        keys = ['key1', 'key2', 'key3']
        values = ['1%2C2%2C3', '1.1%2C2.2%2C3.3', 'a%2Cb%2Cc']
        target = [{
            'key': 'key1',
            'val': {
                '$in': [1, 2, 3]
            }
        },
            {
            'key': 'key2',
            'val': {
                '$in': [1.1, 2.2, 3.3]
            }
        },
            {
            'key': 'key3',
            'val': {
                '$in': ['a', 'b', 'c']
            }
        }]

        key_val_match = helper.getKeyValuePairsExtended(keys, values)
        self.assertEqual(target, key_val_match)

    def test_getKeyValuePairsExtended_SingleValues(self):
        keys = ['key1', 'key2', 'key3', 'key4', 'key5']
        values = ['true', 'false', 'abc', '9.9', '100']
        target = [{
            'key': 'key1',
            'val': {
                  '$eq': True
                  }
        },
            {
            'key': 'key2',
            'val': {
                '$eq': False
            }
        },
            {
            'key': 'key3',
            'val': {
                '$eq': 'abc'
            }
        },
            {
            'key': 'key4',
            'val': {
                '$eq': 9.9
            }
        },
            {
            'key': 'key5',
            'val': {
                '$eq': 100
            }
        }]

        key_val_match = helper.getKeyValuePairsExtended(keys, values)
        self.assertEqual(target, key_val_match)


if __name__ == "__main__":
    unittest.main()
